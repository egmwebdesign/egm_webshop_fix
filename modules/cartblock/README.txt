Custom Cart Block modul

FELADATAI RÖVIDEN:
- létrehoz egy saját custom_cart_block-ot
- ez a blokk megjeleníti a kosárba helyezett elemek árainak összegét

LEÍRÁS:
A Drupal Commerce-es Cart Block nem jeleníti meg a kosárba helyezett termékek összegét, csak a mennyiségét. Ebben a modulban található Custom Cart Blockot használva az eredeti Cart Block helyett elérhető az oldalon ez a funkcionalitás. A CartBlock.php forrásfájl a Commerce Cart Block másolata, kiegészítve az árak összegének kiszámítására.

HASZNÁLAT
A Cart blokk helyett használjuk az oldalon a Custom Cart blokkot.
