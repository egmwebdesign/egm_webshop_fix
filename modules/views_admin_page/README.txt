Views Admin Page

- nézetekhez hozzáad a Page-hez hasonló "Admin Page" megjelenést. Annyi a különbség, hogy ebben beállítható, hogy az adott oldal az admin sminkkel jelenjen meg. Így nem kell /admin/-al kezdődnie az URL-nek (pl. taxonómiákhoz tartozó egyéb adminisztrációs oldalak létrehozásához.).

TEENDŐK:
Modul bekapcsolása után ahhoz a nézethez, amire az adminisztrációs sminket akarjuk alkalmazni

MŰKÖDÉS:
src/Plugin/views/display/AdminPage:
A kód a https://www.drupal.org/project/drupal/issues/2719797 issue-hoz tartozó patchből készült. Amint ez már bekerült a core-ba, ez a modul új site-okhoz feleslegessé válik, és az addig létrehozott "Admin Page" megjelenésekből core-os Page megjelenésű oldalakat létrehozva kikapcsolható a modul a már működő site-okról is.
