<?php

namespace Drupal\views_admin_page\Plugin\views\display;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\Page;

/**
 * The plugin that handles a full page.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "admin_page",
 *   title = @Translation("Admin Page"),
 *   help = @Translation("Display the view as a page in the administation theme, with a URL and menu links."),
 *   uses_menu_links = TRUE,
 *   uses_route = TRUE,
 *   contextual_links_locations = {"admin_page"},
 *   theme = "views_view",
 *   admin = @Translation("Page")
 * )
 */
class AdminPage extends Page {

  protected function getRoute($view_id, $display_id) {
    $route = parent::getRoute($view_id, $display_id);
    if ($this->getOption('use_admin_theme')) {
      $route->setOption('_admin_route', TRUE);
    }
    return $route;
  }

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['use_admin_theme'] = ['default' => FALSE];
    return $options;
  }

  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    // If the display path starts with 'admin/' the page will be rendered with
    // the Administration theme regardless of the 'use_admin_theme' option
    // therefore, we need to set the summary message to reflect this.
    $display_path = $this->getOption('path');
    if (!empty($display_path) && stripos($display_path, 'admin/') === 0) {
      $admin_theme_text = $this->t("Yes (admin path)");
    }
    elseif ($this->getOption('use_admin_theme')) {
      $admin_theme_text = $this->t("Yes");
    }
    else {
      $admin_theme_text = $this->t("No");
    }

    $options['use_admin_theme'] = [
      'category' => 'page',
      'title' => $this->t('Admin theme'),
      'value' => $admin_theme_text,
      'desc' => $this->t('Use the administration theme when rendering this display.'),
    ];
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    switch ($form_state->get('section')) {
      case 'use_admin_theme':
        $form['#title'] .= $this->t('Administration theme');
        $form['use_admin_theme'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Always use admin theme'),
          '#description' => $this->t('Paths starting with "admin/" use the admin theme even when this option is not checked.'),
          '#default_value' => $this->getOption('use_admin_theme'),
        ];
        $display_path = $this->getOption('path');
        if (!empty($display_path) && stripos($display_path, 'admin/') === 0) {
          $form['use_admin_theme']['#default_value'] = TRUE;
          $form['use_admin_theme']['#attributes'] = ['disabled' => 'disabled'];
        }
        break;
    }
  }

  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    switch ($form_state->get('section')) {
      case 'use_admin_theme':
        $this->setOption('use_admin_theme', $form_state->getValue('use_admin_theme'));
        break;
    }
  }

}
