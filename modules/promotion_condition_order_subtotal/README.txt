Promotion Condition Order Subtotal

FELADATAI:
- hozzáad a Promotion-ökhöz új Conditiont: a megrendelésben szereplő termékek árainak összege. Ebbe a termékárra vonatkozó akciók is beleszámításra kerülnek.

LEÍRÁS:
Azért lett a modul létrehozva, mert commerce_promotion modulban csak a teljes megrendelés összegére van Condition létrehozva (ami az Adjustmenteket, adót, szállítási költségeket is beleszámítja).

src/Plugin/Commerce/Condition/OrderSubtotalPrice
A megrendelésben szereplő termékek árainak összegéhez nyújt feltételt Promotion létrehozásakor. Az összegből levonásra kerülnek a Promotionök akciói, hogy ne az eredeti termékárból legyen számítva, amikor a megrendelésben akciós áron vásárolják a terméket. Az OrderTotalPrice condition kódja alapján készült.

HASZNÁLAT
Promotion szerkesztésekor használjuk az Orderre vonatkozó conditionök közül az 'Order subtotal' opciót!
