Order Edit


FELADATAI:
- kiegészíti az Order Edit formot:
  1. Platobná brána szerkesztési lehetőséggel,
  2. Objednané produkty adjustment szerkesztési lehetőséggel.
- felülírja a rendelés és shipment állapotváltó form elemeit érthetőbbre.

LEÍRÁS:
A modul működéséhez legalább a 8.x-2.12 verziójú Commerce modul szükséges.

1. Platobná brána szerkesztési lehetőség bővítés.
Az order edit form egy több inline formot tartalmazó entity form. Mentéskor az egyes form elemek értékeit a nevei alapján kísérli meg menteni a formhoz tartozó entity objektumhoz. Esetünkben a 'payment_gateway' form elem az, amit az order_edit.module fájl order_edit_form_alter() hookjában beleteszünk az entity formba. Mentéskor tehát a form elem értéke az Order objektumba (mivel ez order entity form) a 'payment_gateway' mezőbe fog íródni.

2. Objednané produkty adjustment szerkesztési lehetőség bővítés.
Ehhez:

 a) először módosítjuk az OrderItem entitást. Az adjustment fieldhez beállítjuk, hogy a formokon is megjelenhet 'commerce_adjustment_default' widgettel. Ez történik az order_edit.module fájl order_edit_entity_base_field_info() függvényében.

 b) ekkor egy cache rebuild után már az Adjustment field beállítható az Order Item formokon. Pl.: /admin/commerce/config/order-item-types/default/edit/form-display

 c) order_edit.module fájl order_edit_entity_type_alter() hookjában módosítjuk az OrderItem entitást annyiban, hogy az 'inline_form' handlert (lásd: Commerce-es OrderItem osztály annotációjában eredetileg) lecseréljük a Commerce-es OrderItemInlineFormról a saját CustomOrderItemInlineFormra (hogy ne kelljen az eredeti commerce modult módosítani, az OrderItemInlineForm mintájára készült egy CustomOrderItemInlineForm osztály kiegészítve a saját kóddal). Ezzel személyre tudjuk szabni a form viselkedését anélkül, hogy a Commerce modult módosítani kéne. Ezen a formon elrejtjük az adjustmentek szerkesztését, ha olyan order itemet szerkesztenek, ami még egy "draft" állapotú orderhez tartozik. Akkor ugyanis még az OrderRefresh lefut és újraszámítja az adjustmenteket. Amint más állapotba lép az Order, többé nem fut le ez az automatikus adjustment számítás, így lehetőséget nyújtunk a felhasználónak, hogy maga hozzá tudjon adni adó-, kedvezmény-féle adjustmenteket.

TODO-féleség
3. Order, Shipment állapot-váltó confirmation form módosításai az order_edit_form_alter() hookban. Szövegek és más oldalra átirányítás van mentés után definiálva benne. Ez a confirmation még a commerce 2.24 és a commerce_shipping 2.0-ban nem előhívható, ezért definiáljuk az entity linket hozzá order_edit_entity_type_alter() hookon keresztül. Ha a későbbiekben bekerül ez a modulokba, ezt a hookot ki lehet majd venni.


HASZNÁLAT:
Modul bekapcsolása után engedélyezzük az Order Item entitások formjain az Adjustment mező megjelenítését. Pl. /admin/commerce/config/order-item-types/default/edit/form-display
Ha nem szerepelne a listában az Adjustment mező, akkor cache rebuildet futtassuk le, majd próbáljuk meg utána!
