<?php

namespace Drupal\order_edit;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\commerce_tax\Event\CustomerProfileEvent;
use Drupal\commerce_tax\Event\TaxEvents;
use Drupal\commerce_tax\Resolver\ChainTaxRateResolverInterface;
use Drupal\commerce_tax\StoreTaxInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Applies taxes to orders during the order refresh process.
 */
class CustomTaxOrderProcessor {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The store tax.
   *
   * @var \Drupal\commerce_tax\StoreTaxInterface
   */
  protected $storeTax;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * A cache of prepared customer profiles, keyed by order ID.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  protected $profiles = [];

  /**
   * The chain tax rate resolver.
   *
   * @var \Drupal\commerce_tax\Resolver\ChainTaxRateResolverInterface
   */
  protected $chainRateResolver;

  /**
   * Constructs a new TaxOrderProcessor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The rounder.
   * @param \Drupal\commerce_tax\StoreTaxInterface $store_tax
   *   The store tax.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RounderInterface $rounder, StoreTaxInterface $store_tax, EventDispatcherInterface $event_dispatcher, ChainTaxRateResolverInterface $chain_rate_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->rounder = $rounder;
    $this->storeTax = $store_tax;
    $this->eventDispatcher = $event_dispatcher;
    $this->chainRateResolver = $chain_rate_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function processOrderItem($order_item, $order) {
    $savedOrder = $order_item->getOrder();
    if (!$savedOrder) {
      $order_item->set('order_id', $order->id());
    }
    $tax_types = $this->getTaxTypes();
    foreach ($tax_types as $tax_type) {
      $taxTypePlugin = $tax_type->getPlugin();
      if ($taxTypePlugin->applies($order) && $tax_type->id() !== 'shipping_vat') {
        // Csak a megadott order itemre legyen ez érvényes.
        $this->applyTaxPluginToOrderItem($order_item, $order, $tax_type, $taxTypePlugin);
      }
    }
    // Don't overcharge a tax-exempt customer if the price is tax-inclusive.
    // For example, a 12 EUR price with 20% EU VAT gets reduced to 10 EUR
    // when selling to customers outside the EU, but only if no other tax
    // was applied (e.g. a Japanese customer paying Japanese tax due to the
    // store being registered to collect tax there).
    $calculation_date = $order->getCalculationDate();
    $store = $order->getStore();
    if ($store->get('prices_include_tax')->value) {
      foreach ($order->getItems() as $order_item) {
        $tax_adjustments = array_filter($order_item->getAdjustments(), function ($adjustment) {
          /** @var \Drupal\commerce_order\Adjustment $adjustment */
          return $adjustment->getType() == 'tax';
        });
        if (empty($tax_adjustments)) {
          $unit_price = $order_item->getUnitPrice();
          $rates = $this->storeTax->getDefaultRates($store, $order_item);
          foreach ($rates as $rate) {
            $percentage = $rate->getPercentage($calculation_date);
            $tax_amount = $percentage->calculateTaxAmount($order_item->getUnitPrice(), TRUE);
            $tax_amount = $this->rounder->round($tax_amount);
            $unit_price = $unit_price->subtract($tax_amount);
          }
          $order_item->setUnitPrice($unit_price, $order_item->isUnitPriceOverridden());
        }
      }
    }
  }

  /**
   * Gets the available tax types.
   *
   * @return \Drupal\commerce_tax\Entity\TaxTypeInterface[]
   *   The tax types.
   */
  protected function getTaxTypes() {
    $tax_type_storage = $this->entityTypeManager->getStorage('commerce_tax_type');
    /** @var \Drupal\commerce_tax\Entity\TaxTypeInterface[] $tax_types */
    $tax_types = $tax_type_storage->loadByProperties(['status' => TRUE]);
    uasort($tax_types, [TaxType::class, 'sort']);

    return $tax_types;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_tax\Plugin\Commerce\TaxType\TaxTypeBase $taxTypePlugin
   *
   * @return void
   */
  protected function applyTaxPluginToOrderItem($order_item, $order, $taxType, $taxTypePlugin) {
    $calculation_date = $order->getCalculationDate();
    $store = $order->getStore();
    $prices_include_tax = $store->get('prices_include_tax')->value;
    $zones = $taxTypePlugin->getZones();

    $customer_profile = $this->resolveCustomerProfile($order_item, $taxTypePlugin, $order);
    if (!$customer_profile) {
      return;
    }

    $rates = $this->resolveRates($order_item, $customer_profile, $taxType, $taxTypePlugin);
    foreach ($rates as $zone_id => $rate) {
      $zone = $zones[$zone_id];
      $percentage = $rate->getPercentage($calculation_date);
      // Stores are allowed to enter prices without tax even if they're
      // going to be displayed with tax, and vice-versa.
      // Now that the rates are known, use them to determine the final
      // unit price (which will in turn finalize the order item total).
      if ($prices_include_tax != $taxTypePlugin->isDisplayInclusive()) {
        $unit_price = $order_item->getUnitPrice();
        $tax_amount = $percentage->calculateTaxAmount($unit_price, $prices_include_tax);
        $tax_amount = $this->rounder->round($tax_amount);
        if ($prices_include_tax && !$taxTypePlugin->isDisplayInclusive()) {
          $unit_price = $unit_price->subtract($tax_amount);
        }
        elseif (!$prices_include_tax && $taxTypePlugin->isDisplayInclusive()) {
          $unit_price = $unit_price->add($tax_amount);
        }
        $order_item->setUnitPrice($unit_price);
      }
      // Now determine the tax amount, taking into account other adjustments.
      $adjusted_total_price = $order_item->getAdjustedTotalPrice(['promotion', 'fee']);
      $tax_amount = $percentage->calculateTaxAmount($adjusted_total_price, $taxTypePlugin->isDisplayInclusive());
      if ($taxTypePlugin->shouldRound()) {
        $tax_amount = $this->rounder->round($tax_amount);
      }

      $order_item->addAdjustment(new Adjustment([
        'type' => 'tax',
        'label' => $zone->getDisplayLabel(),
        'amount' => $tax_amount,
        'percentage' => $percentage->getNumber(),
        'source_id' => $taxType->id() . '|' . $zone->getId() . '|' . $rate->getId(),
        'included' => $taxTypePlugin->isDisplayInclusive(),
      ]));
    }

  }

  /**
   * Resolves the customer profile for the given order item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param \Drupal\commerce_tax\Plugin\Commerce\TaxType\TaxTypeBase $taxTypePlugin
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The customer profile, or NULL if not yet known.
   */
  protected function resolveCustomerProfile(OrderItemInterface $order_item, $taxTypePlugin, $order) {
    $customer_profile = $this->buildCustomerProfile($order, $taxTypePlugin);
    // Allow the customer profile to be altered, per order item.
    $event = new CustomerProfileEvent($customer_profile, $order_item);
    $this->eventDispatcher->dispatch($event, TaxEvents::CUSTOMER_PROFILE);
    $customer_profile = $event->getCustomerProfile();

    return $customer_profile;
  }

  /**
   * Builds a customer profile for the given order.
   *
   * Constructed only for the purposes of tax calculation, never saved.
   * The address comes one of the saved profiles, with the following priority:
   * - Shipping profile
   * - Billing profile
   * - Store profile (if the tax type is display inclusive)
   * The tax number comes from the billing profile, if present.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_tax\Plugin\Commerce\TaxType\TaxTypeBase $taxTypePlugin

   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The customer profile, or NULL if not available yet.
   */
  protected function buildCustomerProfile(OrderInterface $order, $taxTypePlugin) {
    $order_id = $order->id();
    if (!isset($this->profiles[$order_id])) {
      $order_profiles = $order->collectProfiles();
      $address = NULL;
      foreach (['shipping', 'billing'] as $scope) {
        if (isset($order_profiles[$scope])) {
          $address_field = $order_profiles[$scope]->get('address');
          if (!$address_field->isEmpty()) {
            $address = $address_field->getValue();
            break;
          }
        }
      }
      if (!$address && $taxTypePlugin->isDisplayInclusive()) {
        // Customer is still unknown, but prices are displayed tax-inclusive
        // (VAT scenario), better to show the store's default tax than nothing.
        $address = $order->getStore()->getAddress();
      }
      if (!$address) {
        // A customer profile isn't usable without an address. Stop here.
        return NULL;
      }

      $tax_number = NULL;
      if (isset($order_profiles['billing'])) {
        $tax_number = $order_profiles['billing']->get('tax_number')->getValue();
      }
      $profile_storage = $this->entityTypeManager->getStorage('profile');
      $this->profiles[$order_id] = $profile_storage->create([
        'type' => 'customer',
        'uid' => 0,
        'address' => $address,
        'tax_number' => $tax_number,
      ]);
    }

    return $this->profiles[$order_id];
  }

  /**
   * Resolves the tax rates for the given order item and customer profile.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param \Drupal\profile\Entity\ProfileInterface $customer_profile
   *   The customer profile. Contains the address and tax number.
   * @param \Drupal\commerce_tax\Plugin\Commerce\TaxType\TaxTypeBase $taxTypePlugin
   *
   * @return \Drupal\commerce_tax\TaxRate[]
   *   The tax rates, keyed by tax zone ID.
   */
  protected function resolveRates(OrderItemInterface $order_item, ProfileInterface $customer_profile, $taxType, $taxTypePlugin) {
    $zones = $this->resolveZones($order_item, $customer_profile, $taxTypePlugin);
    if (!$zones) {
      return [];
    }

    // Provide the tax type entity to the resolvers.
    $this->chainRateResolver->setTaxType($taxType);
    $rates = [];
    foreach ($zones as $zone) {
      $rate = $this->chainRateResolver->resolve($zone, $order_item, $customer_profile);
      if (is_object($rate)) {
        $rates[$zone->getId()] = $rate;
      }
    }
    return $rates;
  }

  /**
   * Resolves the tax zones for the given order item and customer profile.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param \Drupal\profile\Entity\ProfileInterface $customer_profile
   *   The customer profile. Contains the address and tax number.
   * @param \Drupal\commerce_tax\Plugin\Commerce\TaxType\TaxTypeBase $taxTypePlugin
   *
   * @return \Drupal\commerce_tax\TaxZone[]
   *   The tax zones.
   */
  protected function resolveZones(OrderItemInterface $order_item, ProfileInterface $customer_profile, $taxTypePlugin) {
    $customer_address = $customer_profile->get('address')->first();
    $resolved_zones = $taxTypePlugin->getMatchingZones($customer_address);

    return $resolved_zones;
  }

}
