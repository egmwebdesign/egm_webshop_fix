<?php

namespace Drupal\order_edit\Plugin\Field\FieldWidget;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of 'commerce_adjustment_default'.
 *
 * @FieldWidget(
 *   id = "custom_adjustment_default",
 *   label = @Translation("Adjustment Configurable"),
 *   field_types = {
 *     "commerce_adjustment"
 *   }
 * )
 */
class AdjustmentConfigurableWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'enabled_adjustments' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $types = $this->getAdjustmentTypes();

    $element['enabled_adjustments'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Adjustment'),
      '#default_value' => $this->getSetting('enabled_adjustments'),
      '#description' => $this->t('If nothing is selected, then everything will be available.'),
      '#options' => $types,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $enabledTypes = $this->getEnabledAdjustments();

    if (empty($enabledTypes)) {
      return [
        $this->t('Every adjustment is enabled to use.'),
      ];
    }

    $summary = [];
    $summary[] = $this->t('Enabled adjustments: @adjustments', ['@adjustments' => implode(', ', $enabledTypes)]);
    return $summary;
  }

  /**
   * Returns adjustment types.
   *
   * @returns \Drupal\commerce_order\Adjustment[]
   *   Adjustment Types. Key: adjustment type ID, value: adjustment label.
   */
  protected function getAdjustmentTypes() {
    $types = [];

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.commerce_adjustment_type');

    foreach ($plugin_manager->getDefinitions() as $id => $definition) {
      if (!empty($definition['has_ui'])) {
        $types[$id] = $definition['label'];
      }
    }

    return $types;
  }

  /**
   * Returns enabled adjustment types.
   *
   * @returns \Drupal\commerce_order\Adjustment[]
   *   Adjustment Types. Key: adjustment type ID, value: adjustment label.
   */
  protected function getEnabledAdjustments() {
    $types = $this->getAdjustmentTypes();

    $enabledTypes = [];
    $selectedTypes = $this->getSetting('enabled_adjustments');
    if (is_array($selectedTypes)) {
      foreach ($selectedTypes as $id => $name) {
        if (!empty($name)) {
          $enabledTypes[$id] = $types[$id];
        }
      }
    }

    return $enabledTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Adjustment $adjustment */
    $adjustment = $items[$delta]->value;

    $element['#type'] = 'container';
    $element['#attributes']['class'][] = 'form--inline';
    $element['#attached']['library'][] = 'commerce_price/admin';

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.commerce_adjustment_type');

    $types = [
      '_none' => $this->t('- Select -'),
    ];

    $enabledTypes = $this->getEnabledAdjustments();

    // If the current one is disabled, we always show that option
    // to prevent data loss.
    $currentType = ($adjustment) ? $adjustment->getType() : NULL;

    foreach ($plugin_manager->getDefinitions() as $id => $definition) {
      if (
        (
          isset($id) &&
          $id === $currentType
        ) ||
        (
          !empty($definition['has_ui']) &&
          !empty($enabledTypes) &&
          isset($enabledTypes[$id])
        )
      ) {
        $types[$id] = $definition['label'];
      }
    }

    $element['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $types,
      '#weight' => 1,
      '#default_value' => ($adjustment) ? $adjustment->getType() : '_none',
    ];

    // If this is being added through the UI, the source ID should be empty,
    // and we will want to default it to custom.
    $source_id = ($adjustment) ? $adjustment->getSourceId() : NULL;
    $element['source_id'] = [
      '#type' => 'value',
      '#value' => empty($source_id) ? 'custom' : $source_id,
    ];
    // If this is being added through the UI, the adjustment should be locked.
    // UI added adjustments need to be locked to persist after an order refresh.
    $element['locked'] = [
      '#type' => 'value',
      '#value' => ($adjustment) ? $adjustment->isLocked() : TRUE,
    ];

    $states_selector_name = $this->fieldDefinition->getName() . "[$delta][type]";
    $element['definition'] = [
      '#type' => 'container',
      '#weight' => 2,
      '#states' => [
        'invisible' => [
          'select[name="' . $states_selector_name . '"]' => ['value' => '_none'],
        ],
      ],
    ];
    $element['definition']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#size' => 20,
      '#default_value' => ($adjustment) ? $adjustment->getLabel() : '',
    ];
    $element['definition']['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => ($adjustment) ? $adjustment->getAmount()->toArray() : NULL,
      '#allow_negative' => TRUE,
      '#states' => [
        'optional' => [
          'select[name="' . $states_selector_name . '"]' => ['value' => '_none'],
        ],
      ],
      //'#attributes' => ['class' => ['clearfix']],
    ];
    $element['definition']['percentage'] = [
      '#type' => 'number',
      '#title' => $this->t('Percentage'),
      '#description' => '%',
      '#attributes' => [
        'style' => [
          'width: 12em;',
        ],
      ],
      '#default_value' => ($adjustment && !is_float($adjustment->getPercentage()) && is_numeric($adjustment->getPercentage())) ?
      Calculator::multiply($adjustment->getPercentage(), 100) :
      NULL,
    ];
    $element['definition']['included'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Included in the base price'),
      '#default_value' => ($adjustment) ? $adjustment->isIncluded() : FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      if ($value['type'] == '_none') {
        continue;
      }
      // The method can be called with invalid or incomplete data, in
      // preparation for validation. Passing such data to the Adjustment
      // object would result in an exception.
      if (empty($value['definition']['label'])) {
        $form_state->setErrorByName('adjustments[' . $key . '][definition][label]', $this->t('The adjustment label field is required.'));
        continue;
      }

      $values[$key] = new Adjustment([
        'type' => $value['type'],
        'label' => $value['definition']['label'],
        'amount' => new Price($value['definition']['amount']['number'], $value['definition']['amount']['currency_code']),
        'source_id' => $value['source_id'],
        'included' => $value['definition']['included'],
        'locked' => $value['locked'],
        'percentage' => ($value['definition']['percentage'] === '') ? NULL : Calculator::divide($value['definition']['percentage'], '100'),
      ]);
    }
    return $values;
  }

}
