<?php

namespace Drupal\order_edit\Form;

use CommerceGuys\Intl\Calculator;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Element\InlineEntityForm;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;

/**
 * Defines the inline form for order items.
 */
class CustomOrderItemInlineForm extends EntityInlineForm {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeLabels() {
    $labels = [
      'singular' => t('order item'),
      'plural' => t('order items'),
    ];
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = parent::getTableFields($bundles);
    $fields['unit_price'] = [
      'type' => 'field',
      'label' => t('Unit price'),
      'weight' => 2,
    ];
    $fields['quantity'] = [
      'type' => 'field',
      'label' => t('Quantity'),
      'weight' => 3,
    ];
    $fields['total_price'] = [
      'type' => 'field',
      'label' => t('Total price'),
      'weight' => 4,
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function entityForm(array $entity_form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();

    // Ha a Recalculate gomb megnyomása után változik az Order Item,
    // akkor a mező változások sajnos nem mentődnek bele,
    // ezért stringgé szerializáljuk ott, itt pedig vissza azzá az objektummá,
    // ami akkor volt, benne a módosított mező értékekkel.
    if (!empty($triggering['#parents'])) {
      $ief = $form_state->getTemporaryValue('iefentity' . implode('_', $triggering['#parents']));
      if (!empty($ief)) {
        $entity_form['#entity'] = unserialize($ief);
      }
    }

    $entity_form = parent::entityForm($entity_form, $form_state);

    // A "draft" ordereknél nem mutatjuk az adjustmenteket, mert akkor azok
    // automatikusan létrehozásra kerülnek.
    $this->checkAdjustmentsAvailability($entity_form);

    $entity_form['#entity_builders'][] = [get_class($this), 'populateTitle'];

    if (!empty($entity_form['adjustments']['#access'])) {
      $msg = t('Order is already placed. If you want to add or edit the order items, products, or edit quantities, you need to modify the tax- and promotion prices manually.');
      $entity_form['warning_about_editing_placed_order'] = [
        '#markup' => '<div class="messages messages--warning">' . $msg . '</div>',
        '#weight' => -999,
      ];

      $entity_form['do_recalculate'] = [
        '#type' => 'submit',
        '#value' => t('Recalculate taxes'),
        '#submit' => [[get_class($this), 'doRecalculate']],
        '#name' => 'order_item_recalc_' . $entity_form['#ief_row_delta'],
        '#ajax' => [
          'callback' => 'inline_entity_form_get_element',
          'wrapper' => 'inline-entity-form-' . $entity_form['#ief_id'],
          'disable-refocus' => FALSE,
          'event' => 'click',
          'progress' => [
            'type' => 'throbber',
            'message' => t('Recalculating...'),
          ],
        ],
      ];

      $entity_form['adjustments']['#prefix'] = '<div class="messages messages--warning">' . t('After editing the quantity, you can recalculate the new tax price automatically for this order item by clicking the Recalculate taxes button. But promotion prices need to be adjusted manually.') . '</div>';
    }

    return $entity_form;
  }

  protected function setAccessDeniedForAdjustmentsFormElement(&$entity_form) {
    $entity_form['adjustments']['#access'] = FALSE;
  }

  protected function checkAdjustmentsAvailability(&$entity_form) {
    // If parent order is "draft", hide adjustments container.
    $orderItemEntity = $entity_form['#entity'];

    if ($orderItemEntity instanceof OrderItemInterface) {
      $parentOrder = $orderItemEntity->getOrder();

      if (!isset($parentOrder)) {
        // Megnézzük, az URL orderre mutat-e, amiből megállapítható lenne
        // az order állapota.
        $parameters = \Drupal::routeMatch()->getParameters();
        $parentOrder = $parameters->get('commerce_order');
      }

      if (!$parentOrder instanceof OrderInterface) {
        // Nem tudni, melyik order ez. Elrejtjük, hogy ne legyen belőle
        // esetlegesen probléma.
        $this->setAccessDeniedForAdjustmentsFormElement($entity_form);
      }
      else {
        $orderState = $parentOrder->getState();

        if ($orderState instanceof StateItemInterface && $orderState->getId() === 'draft') {
          $this->setAccessDeniedForAdjustmentsFormElement($entity_form);
        }
      }
    }
  }

  /**
   * Entity builder: populates the order item title from the purchased entity.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function populateTitle($entity_type, OrderItemInterface $order_item, array $form, FormStateInterface $form_state) {
    $purchased_entity = $order_item->getPurchasedEntity();
    if ($order_item->isNew() && $purchased_entity) {
      $order_item->setTitle($purchased_entity->getOrderItemTitle());
    }
  }

  public static function doRecalculate(array $form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();

    $values = $form_state->getValues();
    $userInput = $form_state->getUserInput();

    static::orderItemRecalculationProcessAfterOrderPlaced($triggering, $values, $userInput, $form, $form_state);

    $form_state->setValues($values);
    $form_state->setUserInput($userInput);

    $form_state->setRebuild();
  }

  public static function orderItemRecalculationProcessAfterOrderPlaced($triggering, &$values, &$userInput, array $form, FormStateInterface $form_state) {
    if (empty($triggering['#parents'])) {
      return;
    }

    $valueParents = $triggering['#parents'];
    if (end($valueParents) === 'do_recalculate') {
      array_pop($valueParents);
    }

    $valueIefParents = $valueParents;
    if (end($valueIefParents) === 'form') {
      array_pop($valueIefParents);
    }

    $arrayParents = $triggering['#array_parents'];
    if (end($arrayParents) === 'do_recalculate') {
      array_pop($arrayParents);
    }

    // Kiderítjük a parentek alapján, ez melyik order item a formon.
    $formValues = NestedArray::getValue($values, $valueParents);
    $entity_form = NestedArray::getValue($form, $arrayParents);

    // A mennyiséget.
    $quantity = $formValues['quantity'][0]['value'];

    // Mintha beküldenénk a formot, hogy az entity objectbe
    // bekerüljenek a form elemekből az új értékek,
    // DE még nem mentjük el!
    $inline_form_handler = InlineEntityForm::getInlineFormHandler('commerce_order_item');
    $inline_form_handler->entityFormSubmit($entity_form, $form_state);

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order **/
    $order = $form_state->getFormObject()->getEntity();

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $orderItem */
    $orderItem = $entity_form['#entity'];

    // Remove tax adjustments from the order item, so they can be recalculated.
    // És átszámítjuk a kedvezményt a más darabszám esetén, ha volt olyan.
    // Új akció ára = régi_ár * új_db / régi_db.
    $currentAdjustments = $orderItem->getAdjustments();
    foreach ($currentAdjustments as $delta => $currentAdjustment) {
      if ($currentAdjustment->getType() === 'tax') {
        unset($currentAdjustments[$delta]);
      }
    }
    $orderItem->setAdjustments($currentAdjustments);

    // Az order item recalculateTotalPrice() metódusa protected,
    // ezért a setQuantity() metóduson keresztül hívjuk meg.
    // Különben a végső ár nem számolódik újra.
    // Az adószámítás pedig az order item total price mezőből olvassa ki a
    // nettó értéket.
    $orderItem->setQuantity($quantity);

    // Majd az order item price-ra adót számítunk.
    /** @var \Drupal\order_edit\CustomTaxOrderProcessor $taxOrderProcessor */
    $taxOrderProcessor = \Drupal::service('order_edit.tax_order_processor');
    $taxOrderProcessor->processOrderItem($orderItem, $order);
    $entity_form['#entity'] = $orderItem;

    // Sajnos simán a form state storage-ba mentve a nem mentett entitást
    // módosult mezőinek értékei eltűnnek.
    // Ezért máshogy kell nekünk ezt elmenteni.
    $form_state->setTemporaryValue(
      'iefentity' . implode('_', $triggering['#parents']),
      serialize($orderItem)
    );

    // Most nekünk kell csinálni azt, hogy ami az objektumban át lett írva,
    // az kerüljön rá a formra is.
    // Ezt itt úgy csináljuk, hogy a form state-ből a mentett form értékeket
    // kidobjuk, ami erre az inline formra vonatkozik.
    // Így pl.:
    //unset($userInput["order_items"]["form"]["inline_entity_form"]["entities"][0]);
    //unset($values["order_items"]["form"]["inline_entity_form"]["entities"][0]);
    NestedArray::unsetValue($values, $valueIefParents);
    NestedArray::unsetValue($userInput, $valueIefParents);
  }

}
