<?php

namespace Drupal\egm_commerce_email;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the commerce_email.email_sender service.
 */
class EgmCommerceEmailServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Note: it's safest to use hasDefinition() first, because getDefinition()
    // will throw an exception if the given service doesn't exist.
    if ($container->hasDefinition('commerce_email.email_sender')) {
      $definition = $container->getDefinition('commerce_email.email_sender');
      $definition->setClass('Drupal\egm_commerce_email\EmailSender');
    }
  }

}