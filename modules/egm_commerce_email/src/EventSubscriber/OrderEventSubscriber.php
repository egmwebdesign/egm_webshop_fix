<?php

namespace Drupal\egm_commerce_email\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * OrderEventSubscriber class.
 */
class OrderEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // The format for adding a state machine event to subscribe to is:
    // {group}.{transition key}.pre_transition or
    // {group}.{transition key}.post_transition
    // depending on when you want to react.
    $events = [
      //'commerce_order.cart_entity_added.pre_transition' => 'onCartItemAdd',
      //'commerce_order.cart_item_removed.pre_transition' => 'onCartItemRemove',
      'commerce_order.place.pre_transition' => 'onPlaceOrder',
      //'commerce_order.receive_order.pre_transition' => 'onOrderReceived',
      //'commerce_order.create_invoice.pre_transition' => 'onInvoiceCreate',
      //'commerce_order.ship_order.pre_transition' => 'onOrderShipped',
      //'commerce_order.complete.pre_transition' => 'onComplete',
      //'commerce_order.claim.pre_transition' => 'onClaim',
      //'commerce_order.cancel.pre_transition' => 'onCancel',
      //'commerce_order.resend_package.pre_transition' => 'onResendPackage',
      //'commerce_order.claim_resolved.pre_transition' => 'onClaimResolved',
      ];
    return $events;
  }

  /**
   * Elmentjük rendelés pillanatában milyen nyelven használta az oldalat.
   * 
   * Anonim felhasználók esetén ez a további e-mailek nyelvét jelenti majd.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   A workflowban található átmenet esemény.
   */
  public function onPlaceOrder(WorkflowTransitionEvent $event) {
    $orderEntity = $event->getEntity();
    $orderEntity->setData('checkout_langcode', \Drupal::languageManager()->getCurrentLanguage()->getId());
  }

}
