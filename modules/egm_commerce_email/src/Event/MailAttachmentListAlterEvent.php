<?php

namespace Drupal\egm_commerce_email\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 */
class MailAttachmentListAlterEvent extends Event {

  const EVENT_NAME = 'egm_commerce_email.attachment_list_alter';

  protected $attachmentTypes;

  public function __construct($attachmentTypes) {
    $this->attachmentTypes = $attachmentTypes;
  }

  /**
   * @return mixed
   */
  public function getAttachmentTypes() {
    return $this->attachmentTypes;
  }

  /**
   * @param mixed $attachmentTypes
   *
   * @return MailAttachmentListAlterEvent
   */
  public function setAttachmentTypes($attachmentTypes) {
    $this->attachmentTypes = $attachmentTypes;
    return $this;
  }

}
