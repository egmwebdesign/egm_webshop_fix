<?php

namespace Drupal\egm_commerce_email\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 */
class MailAttachmentAlterEvent extends Event {

  const EVENT_NAME = 'egm_commerce_email.attachment_alter';

  protected $attachmentSetting;

  protected $attachments;

  protected $email;

  protected $entity;

  protected $related_entities;

  /**
   * @param string|null $attachmentSetting
   *   Selected attachment generator.
   * @param array $attachments
   *   Attachment files. To add, here is an example code:
   *   $attachment = [
   *    'filecontent' => $pdfFileGenerateResult['pdf'],
   *    'filename' => 'faktura.pdf',
   *    'filemime' => 'application/pdf',
   *   ];
   *   $attachments[] = $attachment;
   * @param \Drupal\commerce_email\Entity\EmailInterface $email
   *   The email.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $related_entities
   *   The related entities.
   */
  public function __construct($attachmentSetting, $attachments, $email, $entity, $related_entities) {
    $this->attachmentSetting = $attachmentSetting;
    $this->attachments = $attachments;
    $this->email = $email;
    $this->entity = $entity;
    $this->related_entities = $related_entities;
  }

  /**
   * @return mixed
   */
  public function getAttachmentSetting() {
    return $this->attachmentSetting;
  }

  /**
   * @return mixed
   */
  public function getAttachments() {
    return $this->attachments;
  }

  /**
   * @param mixed $attachments
   *
   * @return MailAttachmentAlterEvent
   */
  public function setAttachments($attachments) {
    $this->attachments = $attachments;
    return $this;
  }

  /**
   * @return \Drupal\commerce_email\Entity\EmailInterface
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   */
  public function getRelatedEntities() {
    return $this->related_entities;
  }

}
