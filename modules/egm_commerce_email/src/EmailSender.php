<?php

namespace Drupal\egm_commerce_email;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_email\EmailSenderInterface;
use Drupal\commerce_email\Entity\EmailInterface;
use Drupal\commerce_log\LogTemplateManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\Core\Utility\Token;
use Drupal\egm_commerce_email\Event\MailAttachmentAlterEvent;

class EmailSender implements EmailSenderInterface {

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The log template manager.
   *
   * @var \Drupal\commerce_log\LogTemplateManagerInterface
   */
  protected $logTemplateManager;

  /**
   * Constructs a new EmailSender object.
   *
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(MailHandlerInterface $mail_handler, Token $token, Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->mailHandler = $mail_handler;
    $this->token = $token;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Set the log template manager.
   *
   * @param \Drupal\commerce_log\LogTemplateManagerInterface $log_template_manager
   *   The log template manager.
   */
  public function setLogTemplateManager(LogTemplateManagerInterface $log_template_manager) {
    $this->logTemplateManager = $log_template_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function send(EmailInterface $email, ContentEntityInterface $entity, array $related_entities = []) {
    $result = FALSE;
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $default_langcode = $langcode;

    try {
      $entity_type_id = $entity->getEntityTypeId();
      $event_entity_type_id = $email->getEvent()->getEntityTypeId();
      if ($entity_type_id != $event_entity_type_id) {
        throw new \InvalidArgumentException(sprintf('The email requires a "%s" entity, but a "%s" entity was given.', $event_entity_type_id, $entity_type_id));
      }

      $store = NULL;

      if ($entity instanceof OrderInterface) {
        $customer = $entity->getCustomer();

        $store = $entity->getStore();
        $isLangcodeSetByStore = FALSE;

        if (!empty($store) && $store->hasField('field_email_langcode')) {
          // Store milyen nyelvű, olyan lesz a mail is.
          $storeEmailForcedLangcode = $store->get('field_email_langcode')->getValue()[0]['target_id'] ?? NULL;

          if (!empty($storeEmailForcedLangcode) &&
            $storeEmailForcedLangcode !== 'zxx' &&
            $storeEmailForcedLangcode !== 'und'
          ) {
            $langcode = $storeEmailForcedLangcode;
            $isLangcodeSetByStore = TRUE;
          }

        }

        if (!$isLangcodeSetByStore) {
          if ($customer->isAuthenticated()) {
            $langcode = $customer->getPreferredLangcode();
          }
          elseif (!empty($entity->getData('checkout_langcode'))) {
            $langcode = $entity->getData('checkout_langcode');
          }
        }

      }

      if ($default_langcode !== $langcode) {
        $this->setActiveLanguage($langcode);
      }

      // Újból be kell tölteni a configot azon a nyelven, ha van olyan.
      $emailId = $email->id();
      $email_storage = \Drupal::entityTypeManager()->getStorage('commerce_email');
      /** @var \Drupal\commerce_email\Entity\EmailInterface[] $emails */
      $email = $email_storage->load($emailId);

      // An array of keyed entities used to replace tokens.
      $replacements = [$entity_type_id => $entity];
      if (!empty($related_entities)) {
        $related_entity_type_ids = $email->getEvent()->getRelatedEntityTypeIds();
        $replacements = array_merge($replacements, array_combine($related_entity_type_ids, $related_entities));
      }

      $short_entity_type_id = str_replace('commerce_', '', $entity_type_id);
      $to = $this->prepareToString($email, $replacements, $langcode);
      if (empty($to)) {
        return FALSE;
      }
      $subject = $this->replaceTokens($email->getSubject(), $replacements, $langcode);
      $body = [
        '#type' => 'inline_template',
        '#template' => $this->replaceTokens($email->getBody(), $replacements, $langcode),
        '#context' => [
          $short_entity_type_id => $entity,
        ],
      ];

      // Mellékleteket csinálunk fájl mezőkre mutató tokenekből.
      $attachments = [];
      $attachmentsFileField = $email->getThirdPartySetting('egm_commerce_email', 'attachments');
      $isUseCommerceStoreInstead = (
        is_string($attachmentsFileField) &&
        strpos($attachmentsFileField, 'commerce_store-') === 0
      );

      $entityToUseForChecking = ($isUseCommerceStoreInstead) ? $store : $entity;
      $fieldNameToUseForSending = ($isUseCommerceStoreInstead) ?
        str_replace('commerce_store-', '', $attachmentsFileField) :
        $attachmentsFileField;

      if (!empty($fieldNameToUseForSending) &&
        !empty($entityToUseForChecking) &&
        $entityToUseForChecking->hasField($fieldNameToUseForSending)
      ) {
        $attachmentFiles = $entityToUseForChecking->get($fieldNameToUseForSending)->referencedEntities() ?? [];

        foreach ($attachmentFiles as $defaultAttachment) {
          $attachments[] = [
            'filepath' => $defaultAttachment->getFileUri(),
            'filename' => $defaultAttachment->getFilename(),
            'filemime' => $defaultAttachment->getMimeType(),
          ];
        }
      }

      // Enable other modules to alter attachments.
      $event = new MailAttachmentAlterEvent($attachmentsFileField, $attachments, $email, $entity, $related_entities);
      /** @var \Symfony\Component\EventDispatcher\EventDispatcher $event_dispatcher */
      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event_dispatcher->dispatch($event, MailAttachmentAlterEvent::EVENT_NAME);
      $attachments = $event->getAttachments();

      // @todo Figure out how to get the langcode generically.
      $params = [
        'id' => 'commerce_email_' . $email->id(),
        'from' => $this->replaceTokens($email->getFrom(), $replacements, $langcode),
        'cc' => $this->replaceTokens($email->getCc(), $replacements, $langcode),
        'bcc' => $this->replaceTokens($email->getBcc(), $replacements, $langcode),
        'langcode' => $langcode,
      ];

      if (!empty($attachments)) {
        $params['attachments'] = $attachments;
      }

      if ($default_langcode !== $langcode) {
        $this->setActiveLanguage($default_langcode);
      }

      $reply_to = $email->getReplyTo();
      if (!empty($reply_to)) {
        $params['reply-to'] = $this->replaceTokens($reply_to, $replacements, $langcode);
      }

      $result = $this->mailHandler->sendMail($to, $subject, $body, $params);

      // If the email is configured to log sends to the entity.
      if ($email->getLogToEntity() && $this->logTemplateManager !== NULL) {
        // Specify a template ID based on the send result.
        $template_id = $result ? 'mail_' . $email->id() : 'mail_' . $email->id() . '_failure';

        // Look for a matching log template.
        $definitions = $this->logTemplateManager->getDefinitions();
        if (!isset($definitions[$template_id])) {
          // For order emails, fall back to generic templates.
          if ($event_entity_type_id === 'commerce_order') {
            $template_id = $result ? 'order_mail' : 'order_mail_failure';
          }
          else {
            return $result;
          }
        }

        // Log the email to the given entity.
        $log_params = [
          'id' => $email->label(),
          'to_email' => $to,
        ];

        /** @var \Drupal\commerce_log\LogStorageInterface $log_storage */
        $log_storage = $this->entityTypeManager->getStorage('commerce_log');
        $log_storage->generate($entity, $template_id, $log_params)->save();
      }

    }
    catch (\Throwable $e) {
      \Drupal::logger('egm_commerce_email')->error($e->getMessage() . '<br>' . $e->getTraceAsString());
    }
    finally {
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($default_langcode !== $langcode) {
        $this->setActiveLanguage($default_langcode);
      }
    }

    return $result;
  }

  /**
   * Replaces tokens in the given value.
   *
   * @param string $value
   *   The value.
   * @param array $replacements
   *   An array of keyed entities.
   * @param string $langcode
   *   Language code.
   *
   * @return string
   *   The value with tokens replaced.
   */
  protected function replaceTokens(string $value, array $replacements, $langcode) {
    if (!empty($value)) {
      $value = $this->token->replace(
        $value,
        $replacements,
        [
          'langcode' => $langcode,
          'clear' => TRUE,
        ]
      );
    }

    return $value;
  }

  /**
   * Sets the active language for translations.
   *
   * @param string $langcode
   *   The langcode.
   */
  protected function setActiveLanguage($langcode) {
    $languageManager = \Drupal::languageManager();
    $languageDefault = \Drupal::service('language.default');
    $string_translation = \Drupal::service('string_translation');
    if ($languageManager->isMultilingual()) {
      $language = $languageManager->getLanguage($langcode);
      if ($language) {
        // The language manager has no method for overriding the default
        // language, like it does for config overrides. We have to change the
        // default language service's current language.
        // @see https://www.drupal.org/project/drupal/issues/3029010
        $languageDefault->set($language);

        $languageManager->setConfigOverrideLanguage($language);
        $languageManager->reset();

        // The default string_translation service, TranslationManager, has a
        // setDefaultLangcode method. However, this method is not present on
        // either of its interfaces. Therefore we check for the concrete class
        // here so that any swapped service does not break the application.
        // @see https://www.drupal.org/project/drupal/issues/3029003
        if ($string_translation instanceof TranslationManager) {
          $string_translation->setDefaultLangcode($language->getId());
          $string_translation->reset();
        }
      }
    }
  }

  /**
   * Prepares value for the 'to' address.
   *
   * @param \Drupal\commerce_email\Entity\EmailInterface $email
   *   The email.
   * @param array $replacements
   *   An array of keyed entities.
   * @param string $langcode
   *   Language code.
   *
   * @return string
   *   Value for the 'to' address.
   */
  protected function prepareToString(EmailInterface $email, array $replacements, $langcode) {
    if ($email->getToType() === 'role' && !empty($email->getToRole())) {
      /** @var \Drupal\Core\Database\Query\SelectInterface $select */
      $select = $this->database->select('users_field_data', 'd');
      $select->join('user__roles', 'r', 'r.entity_id = d.uid');
      $select->condition('d.status', 1);
      $select->condition('r.roles_target_id', $email->getToRole());
      $select->fields('d', ['mail']);
      $select->groupBy('d.mail');
      $result = $select->execute()->fetchCol();
      $to = implode(', ', $result);
    }
    else {
      $to = $this->replaceTokens($email->getTo(), $replacements, $langcode);
    }
    return $to;
  }

}
