<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\egm_commerce_email\Event\MailAttachmentListAlterEvent;

/**
 * Implements hook_form_alter().
 */
function egm_commerce_email_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'commerce_email_edit_form' ||
    $form_id == 'commerce_email_add_form' ||
    $form_id == 'commerce_email_duplicate_form'
  ) {
    $mail = $form_state->getFormObject()->getEntity();

    $orderFieldOptions = [
      '_none' => t('- None -'),
    ];
    /** @var \Drupal\field\FieldStorageConfigStorage $fieldStorage */
    $fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $orderFields = $fieldStorage->loadByProperties([
      'entity_type' => 'commerce_order',
      'type' => 'file',
      'deleted' => FALSE,
      'status' => 1,
    ]);
    foreach ($orderFields as $orderField) {
      $orderFieldOptions[$orderField->getName()] = t('Order') . ': ' . $orderField->getName();
    }

    $storeFields = $fieldStorage->loadByProperties([
      'entity_type' => 'commerce_store',
      'type' => 'file',
      'deleted' => FALSE,
      'status' => 1,
    ]);
    foreach ($storeFields as $storeField) {
      $orderFieldOptions['commerce_store-' . $storeField->getName()] = t('Store') . ': ' . $storeField->getName();
    }

    // Enable other modules to alter attachments.
    $event = new MailAttachmentListAlterEvent($orderFieldOptions);
    /** @var \Symfony\Component\EventDispatcher\EventDispatcher $event_dispatcher */
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch($event, MailAttachmentListAlterEvent::EVENT_NAME);
    $orderFieldOptions = $event->getAttachmentTypes();

    $form['attachments'] = [
      '#type' => 'select',
      '#title' => t('Attachments'),
      '#options' => $orderFieldOptions,
      '#default_value' => $mail->getThirdPartySetting('egm_commerce_email', 'attachments'),
      '#empty_value' => '_none',
    ];
    $form['#entity_builders'][] = 'egm_commerce_email_product_attribute_edit_form_builder';
  }
}

/**
 * Entity builder for the commerce_product_attribute config entity.
 */
function egm_commerce_email_product_attribute_edit_form_builder($entity_type, \Drupal\commerce_email\Entity\Email $mail, &$form, FormStateInterface $form_state) {
  if ($form_state->getValue('attachments')) {
    $mail->setThirdPartySetting('egm_commerce_email', 'attachments', $form_state->getValue('attachments'));
    return;
  }
  $mail->unsetThirdPartySetting('egm_commerce_email', 'attachments');
}
