<?php

namespace Drupal\custom_order_log\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'OrderLogUserFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "order_log_user_formatter",
 *   label = @Translation("Order Log User"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class OrderLogUserFormatterFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $parentEntity = $items->getEntity();

    $isCron = FALSE;
    if ($parentEntity && $parentEntity->hasField('params')) {
      $params = $parentEntity->get('params')->getValue();
      if (!empty($params[0]['is_cron'])) {
        $isCron = TRUE;
      }
    }

    if ($isCron) {
      $elements[0] = [
        '#markup' => $this->t('System'),
        '#cache' => [
          'tags' => $parentEntity->getCacheTags(),
        ],
      ];
      return $elements;
    }

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      /** @var $entity \Drupal\user\UserInterface */
      $elements[$delta] = [
        '#theme' => 'username',
        '#account' => $entity,
        '#link_options' => ['attributes' => ['rel' => 'author']],
        '#cache' => [
          'tags' => $entity->getCacheTags(),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'user';
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return $entity->access('view label', NULL, TRUE);
  }

}
