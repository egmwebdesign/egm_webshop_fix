<?php

namespace Drupal\shipping_translate_fix\EventSubscriber;

use Drupal\commerce_shipping\Event\ShippingEvents;
use Drupal\commerce_shipping\Event\ShippingRatesEvent;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Changes non-translatable labels to the translatable ones.
 */
class ShippingLabelSubsciber implements EventSubscriberInterface {

  /**
   * Override the rate label and rate description conditionally.
   *
   * The shipping method plugins should be set to non translatable.
   * This avoids needing to translate all rate values and manage these against
   * each translation.
   *
   * @param \Drupal\commerce_shipping\Event\ShippingRatesEvent $event
   *   The shipping rates event.
   */
  public function overrideRateLabelAndDescription(ShippingRatesEvent $event): void {
    $shippingMethod = $event->getShippingMethod();
    $rates = $event->getRates();

    $newRates = [];
    foreach ($rates as $rate) {
      $newRates[] = new ShippingRate(
        [
          'shipping_method_id' => $rate->getShippingMethodId(),
          'service' => new ShippingService(
            $rate->getService()->getId(),
            $shippingMethod->label()
          ),
          'amount' => $rate->getAmount(),
          'description' => $rate->getDescription(),
        ]
      );
    }

    $event->setRates($newRates);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ShippingEvents::SHIPPING_RATES][] = ['overrideRateLabelAndDescription'];
    return $events;
  }

}
