Shipping Translate Fix

- kicseréli a szállítási mód kijelzésben a használt mezőt. A rate label nem fordítható, így a shipping method name mezőt használjuk inkább.

TEENDŐK:
Modul bekapcsolása után /admin/commerce/shipping-methods oldalon rendberakni a fordításokat.
