<?php

/**
 * @file
 * Show which label is shown to customers on the shipping method edit form.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function shipping_translate_fix_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (in_array($form_id, [
    'commerce_shipping_method_add_form',
    'commerce_shipping_method_edit_form',
  ]) &&
    isset($form["plugin"]["widget"][0]["target_plugin_configuration"]["form"]["rate_label"]["#description"])
  ) {
    $form['name']['widget'][0]['value']["#description"] =
      $form["plugin"]["widget"][0]["target_plugin_configuration"]["form"]["rate_label"]["#description"];
    $form["plugin"]["widget"][0]["target_plugin_configuration"]["form"]["rate_label"]["#description"] = '';
  }
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function shipping_translate_fix_field_formatter_info_alter(array &$info) {
  // Use shipping method name instead of Rate label.
  // The rate label is not translatable, so it is useless on multi-language
  // sites.
  $info['commerce_shipping_method']['class'] = 'Drupal\shipping_translate_fix\Plugin\Field\FieldFormatter\ShippingMethodFormatter';
}
