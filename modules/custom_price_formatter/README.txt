Custom Price Formatter

FELADATAI:
- Price formattert hoz létre a DPH nélküli ár kijelzésére termék-, faktúra- és megrendelés adatait listázó oldalakhoz
- ebből létezik HTML-kimenetű és sima szöveges kimenetű változat is

LEÍRÁS:
config/schema/custom_price_formatter.schema.yml
A formatter beállításaihoz kapcsolódó beállítások sémáját írja elő.

src/Plugin/Field/FieldFormatter/CustomPriceCalculatedFormatter osztály:
A 'custom_price_calculated' Price field formatter implementációja. A commerce-es 'price_calculated' formatter lett lemásolva. Kiegészítve lett egy taxDisplayOptions beállítással, amivel beállítható, hogy a Price field hogy kerüljön kijelzésre a DPH-ra nézve:
- Display price without VAT: jelezze ki az árat adók nélkül.
- Display label for price without VAT: ha az előző be van pipálva, akkor jelenítsen meg címkét is, hogy ez az adók nélküli nettó ár.
- Display VAT calculated from price: jelezze ki az adó mennyiségét.
- Display label for VAT calculated from price: ha az előző be van pipálva, akkor jelenítsen meg címkét is, hogy ez csak az adó.
- Display price with VAT: jelezze ki a bruttó árat adókkal.
- Display label for price with VAT: ha az előző be van pipálva, akkor jelenítsen meg címkét is, hogy ez a bruttó ár.

- Use grouping: ezres elválasztókat tesz be.
A formatter elérhető ProductVariation, OrderItem, Order típusú entitások Price fieldjeihez.

A formatter egy template-et irat ki. A modul templates/custom-price-calculated.html.twig fájlt lemásolva lehet saját template-et készíteni rá.

src/Plugin/Field/FieldFormatter/CustomPriceCalculatedFormatter osztály:
A 'custom_price_calculated_text' Price field formatter implementációja. Ugyanúgy generálja ki az árat, mint a CustomPriceCalculatedFormatter, csak a kimenetbe nincsenek HTML-elemek. Hasznos, ha admin felületen vagy feedben kell kiiratni a dolgokat.

HASZNÁLAT
Termékek, orderek, faktúrák Price típusú mezőin állítsuk át a megjelenési formattert 'Custom calculated price'-ra.
