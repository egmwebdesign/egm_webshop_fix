<?php

namespace Drupal\custom_price_formatter\Plugin\Field\FieldFormatter;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\egm_webshop_fix\OrderHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use CommerceGuys\Intl\Currency\CurrencyRepository;

/**
 * Plugin implementation of the 'custom_price_calculated' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_price_calculated",
 *   module = "custom_price_formatter",
 *   label = @Translation("Custom Calculated"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class CustomPriceCalculatedFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  protected $currency_display_options;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The adjustment type manager.
   *
   * @var \Drupal\commerce_order\AdjustmentTypeManager
   */
  protected $adjustmentTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * The price calculator.
   *
   * @var \Drupal\commerce_order\PriceCalculatorInterface
   */
  protected $priceCalculator;

  /**
   * The adjustment transformer.
   *
   * @var \Drupal\commerce_order\AdjustmentTransformerInterface
   */
  protected $adjustmentTransformer;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The library's currency repository.
   *
   * @var \CommerceGuys\Intl\Currency\CurrencyRepositoryInterface
   */
  protected $externalRepository;

  protected $orderHelper;

  /**
   * Constructs a new PriceCalculatedFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\commerce_order\AdjustmentTypeManager $adjustment_type_manager
   *   The adjustment type manager.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\commerce_order\PriceCalculatorInterface $price_calculator
   *   The price calculator.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, CurrencyFormatterInterface $currency_formatter, AdjustmentTypeManager $adjustment_type_manager, CurrentStoreInterface $current_store, AccountInterface $current_user, PriceCalculatorInterface $price_calculator, AdjustmentTransformerInterface $adjustment_transformer, OrderHelperInterface $orderHelper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->adjustmentTypeManager = $adjustment_type_manager;
    $this->currentStore = $current_store;
    $this->currentUser = $current_user;
    $this->priceCalculator = $price_calculator;
    $this->currencyFormatter = $currency_formatter;
    $this->adjustmentTransformer = $adjustment_transformer;
    $this->externalRepository = new CurrencyRepository();

    $this->currency_display_options = [
      'symbol' => $this->t('Symbol (e.g. "$")'),
      'code' => $this->t('Currency code (e.g. "USD")'),
      'none' => $this->t('None'),
    ];

    $this->orderHelper = $orderHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('commerce_price.currency_formatter'),
      $container->get('plugin.manager.commerce_adjustment_type'),
      $container->get('commerce_store.current_store'),
      $container->get('current_user'),
      $container->get('commerce_order.price_calculator'),
      $container->get('commerce_order.adjustment_transformer'),
      $container->get('egm_webshop_fix.order_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'strip_trailing_zeroes' => FALSE,
      'currency_display' => 'symbol',
      'adjustment_types' => [],
      'show_price_without_vat' => FALSE,
      'label_without_vat' => FALSE,
      'show_price_vat_only' => FALSE,
      'label_vat_only' => FALSE,
      'show_price_with_vat' => TRUE,
      'label_with_vat' => FALSE,
      'use_grouping' => FALSE,
      'show_list_price' => FALSE,
      'label_list_price' => FALSE,
      'list_price_apply_taxes' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['strip_trailing_zeroes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strip trailing zeroes after the decimal point.'),
      '#default_value' => $this->getSetting('strip_trailing_zeroes'),
    ];

    $elements['currency_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Currency display'),
      '#options' => $this->currency_display_options,
      '#default_value' => $this->getSetting('currency_display'),
    ];

    $elements['adjustment_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Adjustments'),
      '#options' => [],
      '#default_value' => $this->getSetting('adjustment_types'),
    ];

    foreach ($this->adjustmentTypeManager->getDefinitions() as $plugin_id => $definition) {
      if (!in_array($plugin_id, ['custom'])) {
        $label = $this->t('Apply @label to the calculated price', ['@label' => $definition['plural_label']]);
        $elements['adjustment_types']['#options'][$plugin_id] = $label;
      }
    }

    $elements['show_price_without_vat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display price without VAT.'),
      '#default_value' => $this->getSetting('show_price_without_vat'),
    ];

    $elements['label_without_vat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display label for price without VAT.'),
      '#default_value' => $this->getSetting('label_without_vat'),
    ];

    $elements['show_price_vat_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display VAT calculated from price.'),
      '#default_value' => $this->getSetting('show_price_vat_only'),
    ];

    $elements['label_vat_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display label for VAT calculated from price.'),
      '#default_value' => $this->getSetting('label_vat_only'),
    ];

    $elements['show_price_with_vat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display price with VAT.'),
      '#default_value' => $this->getSetting('show_price_with_vat'),
    ];

    $elements['label_with_vat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display label for price with VAT.'),
      '#default_value' => $this->getSetting('label_with_vat'),
    ];

    $elements['use_grouping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use grouping (thousand separators)'),
      '#default_value' => $this->getSetting('use_grouping'),
    ];

    $elements['show_list_price'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display list price (only works for product variation)'),
      '#default_value' => $this->getSetting('show_list_price'),
    ];

    $elements['label_list_price'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display list price label (only works for product variation)'),
      '#default_value' => $this->getSetting('label_list_price'),
    ];

    $elements['list_price_apply_taxes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Apply taxes for list price (only works for product variation)'),
      '#default_value' => $this->getSetting('list_price_apply_taxes'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('strip_trailing_zeroes')) {
      $summary[] = $this->t('Strip trailing zeroes after the decimal point.');
    }
    else {
      $summary[] = $this->t('Do not strip trailing zeroes after the decimal point.');
    }

    $currency_display = $this->getSetting('currency_display');

    $summary[] = $this->t('Currency display: @currency_display.', [
      '@currency_display' => $this->currency_display_options[$currency_display],
    ]);

    $enabled_adjustment_types = array_filter($this->getSetting('adjustment_types'));
    foreach ($this->adjustmentTypeManager->getDefinitions() as $plugin_id => $definition) {
      if (in_array($plugin_id, $enabled_adjustment_types)) {
        $summary[] = $this->t('Apply @label to the calculated price', ['@label' => $definition['plural_label']]);
      }
    }

    if ($this->getSetting('show_price_without_vat')) {
      $summary[] = $this->t('Display price without VAT.');
    }
    if ($this->getSetting('label_without_vat')) {
      $summary[] = $this->t('Display label for price without VAT.');
    }
    if ($this->getSetting('show_price_vat_only')) {
      $summary[] = $this->t('Display VAT calculated from price.');
    }
    if ($this->getSetting('label_vat_only')) {
      $summary[] = $this->t('Display label for VAT calculated from price.');
    }
    if ($this->getSetting('show_price_with_vat')) {
      $summary[] = $this->t('Display price with VAT.');
    }
    if ($this->getSetting('label_with_vat')) {
      $summary[] = $this->t('Display label for price with VAT.');
    }

    if ($this->getSetting('use_grouping')) {
      $summary[] = $this->t('Use grouping (thousand separators)');
    }

    if ($this->getSetting('show_list_price')) {
      $summary[] = $this->t('Display list price (only works for product variation)');
    }
    if ($this->getSetting('label_list_price')) {
      $summary[] = $this->t('Display list price label (only works for product variation)');
    }
    if ($this->getSetting('list_price_apply_taxes')) {
      $summary[] = $this->t('Apply taxes for list price (only works for product variation)');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if (!$items->isEmpty()) {
      $context = new Context($this->currentUser, $this->currentStore->getStore());
      /** @var \Drupal\commerce\PurchasableEntityInterface $purchasable_entity */
      $purchasable_entity = $items->getEntity();
      $adjustment_types = array_filter($this->getSetting('adjustment_types'));

      if ($purchasable_entity instanceof OrderItem) {
        $calculated = $purchasable_entity->getAdjustedTotalPrice();
        $adjustments = $purchasable_entity->getAdjustments();
      }
      elseif ($purchasable_entity instanceof OrderInterface) {
        $calculated = $purchasable_entity->getTotalPrice();
        $adjustments = $purchasable_entity->collectAdjustments();
        $adjustments = $this->adjustmentTransformer->processAdjustments($adjustments);

      } else {
        $result = $this->priceCalculator->calculate($purchasable_entity, 1, $context, $adjustment_types);
        $calculated = $result->getCalculatedPrice();
        $adjustments = $result->getAdjustments();
      }

      $currency_code = $calculated->getCurrencyCode();

      $vat_price = $this->getVatFromAdjustments($adjustments, $currency_code);
      $priceWithoutVat = $calculated->subtract($vat_price);

      $options = $this->getFormattingOptions();

      $listPrice = NULL;
      if ($purchasable_entity instanceof ProductVariationInterface && $purchasable_entity->hasField('list_price')) {
        $listPrice = $purchasable_entity->getListPrice();

        if (!empty($listPrice) && !empty($this->getSetting('list_price_apply_taxes'))) {
          // If the store's prices don't include tax, then calculate and add it
          // to the saved list price.
          $stores = $purchasable_entity->getStores();
          $store = reset($stores);

          if ($store instanceof StoreInterface) {
            $isPriceWithVat = !empty($store->get('prices_include_tax')->getValue()[0]['value']);

            if (!$isPriceWithVat) {
              $listPrice = $this->calculatePriceWithAdjustments($purchasable_entity, $store, $listPrice);
            }

          }

        }
      }

      $tags = $purchasable_entity->getCacheTags();
      $tags[] = 'commerce_promotion_list';

      $elements[0] = [
        '#theme' => 'custom_price_calculated',
        '#show_price_without_vat' => $this->getSetting('show_price_without_vat'),
        '#label_without_vat' => $this->getSetting('label_without_vat'),
        '#price_without_vat' => $this->currencyFormatter->format($priceWithoutVat->getNumber(), $currency_code, $options),
        '#show_price_with_vat' => $this->getSetting('show_price_with_vat'),
        '#label_with_vat' => $this->getSetting('label_with_vat'),
        '#price_with_vat' => $this->currencyFormatter->format($calculated->getNumber(), $currency_code, $options),
        '#show_price_vat_only' => $this->getSetting('show_price_vat_only'),
        '#label_vat_only' => $this->getSetting('label_vat_only'),
        '#price_vat_only' => $this->currencyFormatter->format($vat_price->getNumber(), $currency_code, $options),
        '#parent_entity' => $purchasable_entity,
        '#parent_entity_view_mode' => $this->viewMode,
        '#show_list_price' => $this->getSetting('show_list_price'),
        '#label_list_price' => $this->getSetting('label_list_price'),
        '#list_price' => (!$listPrice) ? $listPrice : $this->currencyFormatter->format($listPrice->getNumber(), $currency_code, $options),
        '#cache' => [
          'tags' => $tags,
          'contexts' => Cache::mergeContexts($purchasable_entity->getCacheContexts(), [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'country',
          ]),
        ],
      ];

    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type_id = $field_definition->getTargetEntityTypeId();
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);

    return ($entity_type_id == 'commerce_order_item' || $entity_type_id == 'commerce_order')
      || $entity_type->entityClassImplements(PurchasableEntityInterface::class);
  }

  /**
   * @param \Drupal\commerce_order\Adjustment[] $adjustments
   * @param string $currency
   *
   * @return \Drupal\commerce_price\Price
   */
  protected function getVatFromAdjustments($adjustments, $currency) {
    /** @var \Drupal\commerce_price\Price $resultAmount */
    $resultAmount = new Price(0, $currency);

    if (isset($adjustments) && count($adjustments) > 0) {
      foreach ($adjustments as $adjustment) {
        if ($adjustment->getType() === 'tax') {
          $resultAmount = $resultAmount->add($adjustment->getAmount());
        }
      }
    }

    return $resultAmount;
  }

  /**
   * Gets the formatting options for the currency formatter.
   *
   * @return array
   *   The formatting options.
   */
  protected function getFormattingOptions() {
    $options = [
      'currency_display' => $this->getSetting('currency_display'),
      'use_grouping' => !empty($this->getSetting('use_grouping')),
    ];
    if ($this->getSetting('strip_trailing_zeroes')) {
      $options['minimum_fraction_digits'] = 0;
    }

    return $options;
  }

  protected function calculatePriceWithAdjustments($productVariation, $store, $price) {
    $quantity = 1;
    $context = $this->orderHelper->getContext($store);
    $order = $this->orderHelper->createFakeOrderForCalculations($productVariation, $quantity, $context, $price);

    // Csak a promotionöket vesszük számításba.
    $adjustment_types = $this->orderHelper->getAdjustmentTypes(FALSE, FALSE, TRUE, FALSE, FALSE);
    $calculatedPrice = $this->orderHelper->getFirstProductVariationCalculatedPrice($order, $adjustment_types);
    return $calculatedPrice;
  }

}
