<?php

namespace Drupal\custom_price_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'custom_price_calculated_text' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_price_calculated_text",
 *   module = "custom_price_formatter",
 *   label = @Translation("Custom Calculated (Text-only)"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class CustomPriceCalculatedFormatterText extends CustomPriceCalculatedFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $result = parent::viewElements($items, $langcode);

    if (isset($result[0]['#theme'])) {
      $result[0]['#theme'] = 'custom_price_calculated_text';
    }

    return $result;
  }

}
