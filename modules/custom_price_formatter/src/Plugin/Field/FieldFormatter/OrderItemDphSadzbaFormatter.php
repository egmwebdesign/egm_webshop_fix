<?php

namespace Drupal\custom_price_formatter\Plugin\Field\FieldFormatter;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'custom_order_item_tax_rate_percent' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_order_item_tax_rate_percent",
 *   label = @Translation("Order Item Tax Rate Percent"),
 *   field_types = {
 *     "string",
 *     "integer",
 *     "decimal",
 *     "float",
 *   }
 * )
 */
class OrderItemDphSadzbaFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];
    $orderItem = $items->getEntity();
    if ($orderItem instanceof OrderItemInterface) {
      $sadzba = $this->getOrderDphSadzba($orderItem);
      $build[] = [
        '#markup' => isset($sadzba) ? (100 * $sadzba) . '%' : '',
      ];
    }

    return $build;
  }

  public function getOrderDphSadzba($orderItem) {
    $adjustments = $orderItem->getAdjustments();
    foreach ($adjustments as $adjustment) {
      if ($adjustment->getType() === 'tax' && !empty($adjustment->getPercentage())) {
        return $adjustment->getPercentage();
      }
    }
    return NULL;
  }

}
