<?php

namespace Drupal\promotion_order_shipping_methods\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent;
use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class FilterPaymentGatewaysEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  const IS_FUNCTIONALITY_DISABLED_KEY_IN_DB = 'promotion_order_shipping_methods.is_disabled';

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvents::FILTER_PAYMENT_GATEWAYS => 'filterPaymentGateways',
      KernelEvents::REQUEST => 'redirectToOrderEditIfNoItems',
    ];
  }

  /**
   * Extra conditions for filtering available payment gateways.
   *
   * @param \Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent $event
   *   Event with data.
   */
  public function filterPaymentGateways(FilterPaymentGatewaysEvent $event) {
    if (!empty(\Drupal::state()->get(FilterPaymentGatewaysEventSubscriber::IS_FUNCTIONALITY_DISABLED_KEY_IN_DB))) {
      return;
    }

    // Ha az admin szerkesztő oldalakon vagyunk, akkor itt ne érvényesüljön
    // ez a kritérium. Enélkül az történne, hogy míg nincs Shipment,
    // addig nem tudnak fizetést se hozzáaadni.
    // Ha nincs order item az orderben, akkor az $order->getTotalPrice()
    // NULL-t ad vissza és emiatt hibával leáll a Payment Add form.
    // Úgyhogy oda a redirectToOrderEditIfNoItems() metódussal átirányítjuk
    // ebben az esetben.
    $routeName = \Drupal::routeMatch()->getRouteName();
    $routesToSkipCheck = [
      'entity.commerce_payment.add_form' => 1,
      'entity.commerce_payment.operation_form' => 1,
    ];

    if (!empty($routesToSkipCheck[$routeName])) {
      return;
    }

    $paymentGateways = $event->getPaymentGateways();
    $order = $event->getOrder();

    if (empty($paymentGateways)) {
      return;
    }

    $shippingMethodDefaultValues = \Drupal::state()->get(
      'payment_gateway_custom_order_shipping_methods'
    );

    $plugin_manager = \Drupal::service('plugin.manager.commerce_condition');

    foreach ($paymentGateways as $id => $paymentGateway) {
      $settings = isset($shippingMethodDefaultValues[$id]) ? $shippingMethodDefaultValues[$id] : [];
      $settings = ['shipping_methods' => $settings];

      /** @var \Drupal\promotion_order_shipping_methods\Plugin\Commerce\Condition\OrderShippingMethods $condition */
      $condition = $plugin_manager->createInstance('custom_order_shipping_methods', $settings);
      $conditionResult = $condition->evaluate($order);

      if (!$conditionResult) {
        unset($paymentGateways[$id]);
      }
    }

    $event->setPaymentGateways($paymentGateways);
  }

  /**
   * Redirects to order edit from payment pages if no order items found.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Request event.
   */
  public function redirectToOrderEditIfNoItems(RequestEvent $event) {
    $request = $event->getRequest();
    $routeName = $request->attributes->get('_route');

    $routesToSkipCheck = [
      'entity.commerce_payment.collection' => 1,
      'entity.commerce_payment.add_form' => 1,
      'entity.commerce_payment.operation_form' => 1,
    ];

    if (!empty($routesToSkipCheck[$routeName])) {
      $order = $request->attributes->get('commerce_order');

      if ($order instanceof OrderInterface) {
        $totalPrice = $order->getTotalPrice();

        if (!isset($totalPrice)) {
          \Drupal::messenger()->addWarning(
            $this->t('No order items found for this order. To manage payments, please add one first.')
          );

          $langcode = \Drupal::languageManager()->getCurrentLanguage();

          $response = new TrustedRedirectResponse(
            Url::fromRoute(
              'entity.commerce_order.edit_form',
              ['commerce_order' => $order->id()],
              ['language' => $langcode]
            )->toString()
          );

          $event->setResponse($response);
        }
      }

    }
  }

}
