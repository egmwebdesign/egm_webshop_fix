<?php

namespace Drupal\promotion_order_shipping_methods\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the shipping method condition for orders.
 *
 * @CommerceCondition(
 *   id = "custom_order_shipping_methods",
 *   label = @Translation("Shipping methods"),
 *   display_label = @Translation("Selected shipping method"),
 *   category = @Translation("Order"),
 *   entity_type = "commerce_order",
 * )
 */
class OrderShippingMethods extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shipping_methods' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shipping_methods'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Shipping methods'),
      '#default_value' => $this->configuration['shipping_methods'],
      '#target_type' => 'commerce_shipping_method',
      '#hide_single_entity' => FALSE,
      '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['shipping_methods'] = $values['shipping_methods'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;

    if (empty($order->get('shipments'))) {
      // No shipments, no promotion for shipping.
      return FALSE;
    }

    // Get shipping method IDs used currently.
    $shipments = $order->get('shipments')->getValue();

    if (empty($shipments)) {
      return FALSE;
    }

    $shipping_method_ids = [];
    foreach ($shipments as $shipmentId) {
      $shipment = \Drupal::service('entity_type.manager')->getStorage('commerce_shipment')->load($shipmentId['target_id']);
      $shipping_method_ids[] = $shipment->getShippingMethodId();
    }

    // Check if the shipping methods we are using for the order
    // are among the enabled shipping methods for this promotion.
    $result = !empty(array_intersect($shipping_method_ids, $this->configuration['shipping_methods']));

    return $result;
  }

}
