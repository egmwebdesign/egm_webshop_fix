<?php

namespace Drupal\promotion_order_shipping_methods\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\promotion_order_shipping_methods\EventSubscriber\FilterPaymentGatewaysEventSubscriber;

/**
 * Implements an example form.
 */
class PaymentGatewayShippingMethodForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_gateway_shipping_method_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $shippingMethodDefaultValues = \Drupal::state()->get(
      'payment_gateway_custom_order_shipping_methods'
    );

    if (empty($shippingMethodDefaultValues)) {
      $shippingMethodDefaultValues = [];
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $paymentGateways */
    $paymentGateways = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway')->loadMultiple();

    $form['#tree'] = TRUE;

    $form['is_disabled_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => '',
      '#open' => TRUE,
    ];

    $form['is_disabled_wrapper']['is_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable this payment gateway filtering by shipping method and just use the Conditions from default Drupal Commerce behavior'),
      '#default_value' => \Drupal::state()->get(FilterPaymentGatewaysEventSubscriber::IS_FUNCTIONALITY_DISABLED_KEY_IN_DB) ?? '0',
    ];

    $form['help'] = [
      '#markup' => $this->t('Set up for which shipping methods can each payment gateway be used with. If nothing is selected, then the payment gateway can never be used.<br>These settings will not be saved to the site configuration.'),
    ];

    foreach ($paymentGateways as $id => $paymentGateway) {
      $form['payment_gateways'][$id] = [
        '#type' => 'commerce_entity_select',
        '#title' => $paymentGateway->label(),
        '#default_value' => isset($shippingMethodDefaultValues[$id]) ? $shippingMethodDefaultValues[$id] : [],
        '#target_type' => 'commerce_shipping_method',
        '#autocomplete_threshold' => 999,
        '#hide_single_entity' => FALSE,
        '#multiple' => TRUE,
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $paymentGateways = $form_state->getValue('payment_gateways');
    \Drupal::state()->set('payment_gateway_custom_order_shipping_methods', $paymentGateways);

    $isDisabled = $form_state->getValue('is_disabled_wrapper')['is_disabled'] ?? '0';
    \Drupal::state()->set(FilterPaymentGatewaysEventSubscriber::IS_FUNCTIONALITY_DISABLED_KEY_IN_DB, $isDisabled);

    \Drupal::messenger()->addMessage('The settings have been saved to the database.');
  }

}
