<?php

namespace Drupal\custom_commerce_domain\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Default widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "obchod",
 *   label = @Translation("Store"),
 * )
 */
class ObchodWidget extends FilterWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable($filter = NULL, array $filter_options = []) {
    return TRUE;
  }

  public function exposedFormAlter(array &$form, FormStateInterface $form_state) {
    $field_id = $this->getExposedFilterFieldId();

    parent::exposedFormAlter($form, $form_state);

    if (!empty($form[$field_id])) {
      $stores = \Drupal::entityTypeManager()->getStorage('commerce_store')->loadMultiple();
      $options = [
        '' => t('- All -'),
      ];
      foreach ($stores as $store) {
        $options[$store->id()] = $store->label();
      }
      $form[$field_id]['#options'] = $options;
      $form[$field_id]['#type'] = 'select';
      if (isset($form[$field_id]['#size'])) {
        unset($form[$field_id]['#size']);
      }
      $form[$field_id]['#multiple'] = FALSE;
      $form[$field_id]['#empty_option'] = t('- All -');
    }

  }

}
