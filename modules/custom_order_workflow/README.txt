Custom Order Workflow

FELADATAI:
- a megrendeléshez saját order_custom Workflow (rendelés állapotok és köztük átmenetek) kerül létrehozásra

LEÍRÁS:
custom_order_workflow.workflows.yml:
Itt kerül definiálásra a saját order_custom workflow.

HASZNÁLAT
A használt Order type-nál állítsuk át a Custom order workflowra a postup práce mezőt!
