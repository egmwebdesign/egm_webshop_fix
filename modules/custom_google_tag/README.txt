Custom Google Tags


FELADATAI:
- a Message Blokkban megjelenő tájékoztató üzeneteket küldi el a Google Tag Managernek.


LEÍRÁS:
js/custom_google_tag.js
Oldalbetöltéskor ha megkapta paraméterben az üzeneteket a drupalSettings paraméterben, akkor elküldi a Google Tag Managernek. Ha üres tömböt vagy NULL-t kap, nem csinál semmit.

custom_google_tag.module
A custom_google_tag_page_attachments hookban csatoljuk a js fájlt oldalbetöltéskor, valamint ha az oldalon megjelenne 1 vagy több üzenet, akkor azt átadjuk neki. A 'status' típusú tájékoztató üzenetet nem ad át, csak a többi fajtát (warning, error). Az átadott üzenet felépítése:

['drupalSettings'][] =
    'custom_google_tag' => [
        'dataLayerVariable' => '<dataLayer JS-változó neve, amit a google_tag modul configjából olvassa be>'
        'messages' => [

            0 => [
                'event' => 'userMessages',
                'message' => [
                    'type' => '<message típusa>',
                    'value' => '<üzenet szövege>',
                    'url' => '<jelenlegi teljes URL>',
                    'user' => '<user ID>',
                ]
            ],

            1 => [
                'event' => 'userMessages',
                'message' => [
                    'type' => '<message típusa>',
                    'value' => '<üzenet szövege>',
                    'url' => '<jelenlegi teljes URL>',
                    'user' => '<user ID>',
                ]
            ],

            stb.

        ]
    ]
]


HASZNÁLAT:
Ahhoz, hogy a küldés működjön, a Google Tag Managert kell beállítani:
1. hozzunk létre DataLayer Variable (Adatréteg változó):
    - neve: Message Type
      változó típus: Adatréteg-változó
      Adatréteg-változó neve: message.type
      Adatréteg verziója: 2. verzió
    - neve: Message Value
      változó típus: Adatréteg-változó
      Adatréteg-változó neve: message.value
      Adatréteg verziója: 2. verzió

2. hozzunk létre triggert (aktiválási szabályt):
    - neve: userMessages
    - Aktiválási szabály típusa: Egyéni esemény
    - Eseménynév: userMessages
    - Az aktiválási szabály aktiválása: Minden egyéni esemény

3. hozzunk létre taget (címkét):
    - neve: userMessages - GA
    - Címketípus: Google Analytics - Universal Analytics
    - Nyomon követés típusa: Esemény
    - Eseménykövető paraméterek, Kategória: Error Messages
    - Művelet: {{Message Type}} - {{Message Value}}
    - Címke: {{Page URL}}
      (ha nem lenne ilyen változó, nyissuk meg a Változók oldalt a bal oldali menüből és a Beépített változók közül engedélyezzük a Page URL-t)
    - Érték:
    - Nem interakciós lekérés: Igaz
    - Google Analytics-beállítások: <állítsuk be azt a változót, amiben az Analytics ID van tárolva>
    - Aktiválási szabály: az előbb létrehozott userMessages nevűt állítsuk be!

4. Preview módban letesztelni (ez localhoston nem működött) vagy debugolás Chrome-ban lehetséges a WASP inspector vagy GA Debug pluginnel.

5. Élesíteni a kék Küldés gombbal.