/**
 * @file
 * Defines Javascript behaviors for the custom_google_tag_manager module.
 */

(function ($, window, drupalSettings) {
  'use strict';

  $(function () {
    // If no drupalSettings, exit.
    if (!drupalSettings) {
      return;
    }

    // Get attached custom_google_tag setting.
    var settings = drupalSettings.custom_google_tag || {};

    if (!settings) {
      return;
    }

    // Get dataLayer variable name.
    var dataLayerVariable = settings.dataLayerVariable;

    if (!dataLayerVariable || !window.hasOwnProperty(dataLayerVariable)) {
      return;
    }

    var dataLayer = window[dataLayerVariable];

    // Get message array.
    var messages = settings.messages || {};

    // Push message events to GTM.
    if (messages.length > 0) {
      messages.forEach(function (eventData) {
        dataLayer.push(eventData);
      });
    }

  });
})(jQuery, window, drupalSettings);
