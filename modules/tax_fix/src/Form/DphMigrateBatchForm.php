<?php

namespace Drupal\tax_fix\Form;

use Drupal\commerce_price\Calculator;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\commerce_tax\Entity\TaxTypeInterface;
use Drupal\commerce_tax\Resolver\TaxRateResolverInterface;
use Drupal\commerce_tax\TaxZone;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

class DphMigrateBatchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dph_migrate_batch_form';
  }

  protected function getEmptyLabel() {
    return $this->t('- None -');
  }

  protected function getFieldSetting($setting) {
    $result = [
      'tax_type' => 'vat',
      'allowed_zones' => [],
    ];
    return $result[$setting] ?? NULL;
  }

  protected function getOptions() {
    if (isset($this->options)) {
      return $this->options;
    }
    $tax_type = $this->getFieldSetting('tax_type');
    $this->options = [];

    // Add an empty option if the widget needs one.
    if ($empty_label = $this->getEmptyLabel()) {
      $this->options = ['_none' => $empty_label];
    }
    $no_applicable_tax = TaxRateResolverInterface::NO_APPLICABLE_TAX_RATE;
    // Avoid instantiating the same labels dozens of times.
    $no_applicable_tax_label = $this->t('No tax');

    if (!$tax_type) {
      return $this->options;
    }
    $tax_type = TaxType::load($tax_type);
    /** @var \Drupal\commerce_tax\Plugin\Commerce\TaxType\LocalTaxTypeInterface $tax_type_plugin */
    $tax_type_plugin = $tax_type->getPlugin();

    foreach ($tax_type_plugin->getZones() as $zone) {
      if (!$this->isAllowed($tax_type, $zone)) {
        continue;
      }
      $label = (string) $zone->getLabel();
      $this->options[$label] = [$zone->getId() . '|' . $no_applicable_tax => $no_applicable_tax_label];

      foreach ($zone->getRates() as $rate) {
        $rate_label = $this->t('@label', ['@label' => $rate->getLabel()]);
        if ($percentage = $rate->getPercentage()) {
          $rate_label = $this->t('@label (@percentage%)', [
            '@label' => $rate->getLabel(),
            '@percentage' => Calculator::multiply($percentage->getNumber(), '100'),
          ]);
        }
        $this->options[$label][$zone->getId() . '|' . $rate->getId()] = $rate_label;
      }
    }

    return $this->options;
  }

  /**
   * Checks whether the given tax type and zone are allowed in the widget.
   *
   * @param \Drupal\commerce_tax\Entity\TaxTypeInterface $tax_type
   *   The tax type.
   * @param \Drupal\commerce_tax\TaxZone $zone
   *   The tax zone.
   *
   * @return bool
   *   TRUE if the given tax type and zone are allowed, FALSE otherwise.
   */
  protected function isAllowed(TaxTypeInterface $tax_type, TaxZone $zone) {
    if ($tax_type->getPluginId() == 'european_union_vat' && $zone->getId() == 'ic') {
      // The EU Intra-Community Supply zone is special and never displayed.
      return FALSE;
    }
    $allowed_zones = $this->getFieldSetting('allowed_zones');
    if (empty($allowed_zones)) {
      return TRUE;
    }

    return in_array($zone->getId(), $allowed_zones);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tax_rate'] = [
      '#type' => 'select',
      '#title' => $this->t('Tax rate'),
      '#options' => $this->getOptions(),
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('tax_rate') ?? 'sk|reduced',
    ];

    $form['field_tax_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product variation tax rate field name'),
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('field_tax_rate') ?? 'field_tax_rate',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Start',
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $newTaxRate = $form_state->getValue('tax_rate');
    $taxRateFieldName = $form_state->getValue('field_tax_rate');
    $batch = [
      'title' => 'Set DPH sadzba for all product variations',
      'finished' => [$this, 'batchFinished'],
      'operations' => [
        [
          [$this, 'doBatch'],
          [$taxRateFieldName, $newTaxRate],
        ]
      ],
    ];

    batch_set($batch);
  }

  public static function doBatch($taxRateFieldName, $newTaxRate, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = self::getCount(0);
      $context['results']['changed'] = 0;
    }

    if ($context['sandbox']['max'] == 0) {
      $context['finished'] = TRUE;
      return;
    }

    $limit = 20;
    $query = self::getQuery($context['sandbox']['current_id']);
    $query->range(0, $limit);
    $ids = $query->execute();

    foreach ($ids as $id) {
      $processResult = self::doProcessItem($taxRateFieldName, $newTaxRate, $id);

      if ($processResult) {
        $context['results']['changed'] += 1;
      }

      $context['sandbox']['progress'] += 1;
      $context['sandbox']['current_id'] = $id;

      $context['message'] = t('Progress: @progress / @max', [
        '@progress' => $context['sandbox']['progress'],
        '@max' => $context['sandbox']['max'],
      ]);
    }

    $context['finished'] = (empty($ids) || $context['sandbox']['progress'] >= $context['sandbox']['max']);
  }

  public static function getQuery($id) {
    $query = \Drupal::entityQuery('commerce_product_variation')
      ->condition('variation_id', $id, '>')
      ->sort('variation_id', 'ASC')
      ->accessCheck(FALSE);

    return $query;
  }

  public static function getCount($id) {
    $query = self::getQuery($id);
    return $query->count()->execute();
  }

  public static function doProcessItem($taxRateFieldName, $newTaxRate, $variationId) {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')
      ->load($variationId);
    if (empty($variationId)) {
      return FALSE;
    }

    $currentTaxRate = $variation->get($taxRateFieldName)->getValue()[0]['value'] ?? NULL;
    if ($currentTaxRate !== $newTaxRate) {
      $variation->set($taxRateFieldName, $newTaxRate);
      $variation->save();
      return TRUE;
    }

    return FALSE;
  }

  public static function batchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $message = t('Processed: @processed', [
        '@processed' => $results['changed'],
      ]);
      $messenger->addMessage($message);
    }
    else {
      $messenger->addError('Error while processing.');
    }
  }

}
