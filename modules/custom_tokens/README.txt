Custom Tokens

- Commerce adószámítás felülbírálása.
- akkor is számoljon adókkal, ha nincs Billing Profile egy Orderhez, tehát a termékoldalakon, listákon ki lehessen a DPH-s árakat jelezni.

TEENDŐK:
Modul bekapcsolása után a metageknél a product variation mezők mellett lehet "price_with_taxes_included" (nettó ár a pénznemmel együtt) és "price_number_with_taxes_included" (csak a nettó ár) tokeneket használni.

Pl. product_price_amount metatagre:
[commerce_product_variation:price_number_with_taxes_included]

Vagy 1 variációval rendelkező termékre: product_price_amount:
[commerce_product:variations:0:entity:price_number_with_taxes_included]
