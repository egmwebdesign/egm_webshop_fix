<?php

namespace Drupal\custom_schema_product\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'Action' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "merchant_return_policy",
 *   label = @Translation("MerchantReturnPolicy"),
 *   tree_parent = {
 *     "MerchantReturnPolicy",
 *   },
 *   tree_depth = 0,
 *   property_type = "MerchantReturnPolicy",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "returnPolicyCategory" = {
 *       "id" = "text",
 *       "label" = @Translation("returnPolicyCategory"),
 *       "description" = @Translation("https://schema.org/MerchantReturnEnumeration"),
 *     },
 *     "returnMethod" = {
 *       "id" = "text",
 *       "label" = @Translation("returnMethod"),
 *       "description" = @Translation("https://schema.org/ReturnMethodEnumeration"),
 *     },
 *     "returnFees" = {
 *       "id" = "text",
 *       "label" = @Translation("returnFees"),
 *       "description" = @Translation("https://schema.org/ReturnFeesEnumeration"),
 *     },
 *     "merchantReturnDays" = {
 *       "id" = "number",
 *       "label" = @Translation("merchantReturnDays"),
 *       "description" = @Translation("https://schema.org/merchantReturnDays"),
 *     },
 *   },
 * )
 */
class MerchantReturnPolicy extends PropertyTypeBase {

}
