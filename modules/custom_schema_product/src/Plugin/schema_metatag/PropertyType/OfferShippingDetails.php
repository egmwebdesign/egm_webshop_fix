<?php

namespace Drupal\custom_schema_product\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * @SchemaPropertyType(
 *   id = "offer_shipping_details",
 *   label = @Translation("OfferShippingDetails"),
 *   tree_parent = {
 *     "OfferShippingDetails",
 *   },
 *   tree_depth = 0,
 *   property_type = "OfferShippingDetails",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "shippingRate" = {
 *       "id" = "monetary_amount",
 *       "property_type" = "MonetaryAmount",
 *       "label" = @Translation("shippingRate"),
 *       "tree_parent" = {
 *         "MonetaryAmount",
 *       },
 *       "description" = "",
 *     },
 *     "shippingDestination" = {
 *       "id" = "defined_region",
 *       "label" = @Translation("shippingDestination"),
 *       "property_type" = "DefinedRegion",
 *       "tree_parent" = {
 *         "DefinedRegion",
 *       },
 *       "description" = "",
 *     },
 *     "deliveryTime" = {
 *       "id" = "shipping_delivery_time",
 *       "label" = @Translation("deliveryTime"),
 *       "description" = "",
 *       "property_type" = "ShippingDeliveryTime",
 *       "tree_parent" = {
 *         "ShippingDeliveryTime",
 *       },
 *       "tree_depth" = 0,
 *     },
 *   },
 * )
 */
class OfferShippingDetails extends PropertyTypeBase {}
