<?php

namespace Drupal\custom_schema_product\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'Action' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "defined_region",
 *   label = @Translation("DefinedRegion"),
 *   tree_parent = {
 *     "DefinedRegion",
 *   },
 *   tree_depth = 0,
 *   property_type = "DefinedRegion",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "addressCountry" = {
 *       "id" = "text",
 *       "label" = @Translation("addressCountry"),
 *       "description" = @Translation("SK/AT/CZ"),
 *     },
 *   },
 * )
 */
class DefinedRegion extends PropertyTypeBase {

}
