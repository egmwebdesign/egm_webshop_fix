<?php

namespace Drupal\custom_schema_product\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'Action' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "shipping_delivery_time",
 *   label = @Translation("ShippingDeliveryTime"),
 *   tree_parent = {
 *     "QuantitativeValue",
 *   },
 *   tree_depth = 0,
 *   property_type = "ShippingDeliveryTime",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "handlingTime" = {
 *       "id" = "quantitative_value",
 *       "label" = @Translation("handlingTime"),
 *       "property_type" = "QuantitativeValue",
 *       "tree_parent" = {
 *         "QuantitativeValue",
 *       },
 *       "description" = @Translation(""),
 *     },
 *     "transitTime" = {
 *       "id" = "quantitative_value",
 *       "description" = "",
 *       "label" = @Translation("transitTime"),
 *       "tree_parent" = {
 *         "QuantitativeValue",
 *       },
 *       "property_type" = "QuantitativeValue",
 *     }
 *   },
 * )
 */
class ShippingDeliveryTime extends PropertyTypeBase {

}
