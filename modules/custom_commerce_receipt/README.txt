Custom Commerce Receipt

A rendelésről kiküldött twiggel renderelt számla módosítására készült.

- A számla template-jéhez extra Twig-változók hozzáadhatók a custom_commerce_receipt.module fájl custom_commerce_receipt_preprocess_commerce_order_receipt() hookjában.
- A számla template-je a templates/commerce-order-receipt.html.twig (módosítva van az eredeti a modul által)
