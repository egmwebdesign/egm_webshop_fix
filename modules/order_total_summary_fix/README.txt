Order Total Summary Fix

FELADATAI:
- a megrendelés végösszegét összegző template-et hoz létre, amit a megrendelés és faktúra oldalak használhatnak

LEÍRÁS:
css/order_total_summary_fix.total_summary.css:
A kimeneti szöveg jobbra igazítása.

src/CustomOrderTotalSummary osztály:
A commerce-es OrderTotalSummary alapján készült. Annyi módosítás történt rajta, hogy kijelzésre kerül a DPH nélküli ár. Az osztály service-ként is elérhető: commerce_order.order_total_summary.

templates/commerce-order-total-summary.html.twig
A kimeneti twig-sablon. Előbb az Adjustment-ek (kedvezmények, adó, szállítási költség) kerül kijelzésre, majd utána az adó nélküli ár, majd a végösszeg.

order_total_summary_fix.libraries.yml:
Létrehoz egy library-t a modulban lévő css fájllal.

order_total_summary_fix.module:
Itt van megadva a order_total_summary_fix_theme hookban, hogy mihez használja a commerce-order-total-summary twig template-et.

HASZNÁLAT
A modul bekapcsolása után nincs más teendő.
