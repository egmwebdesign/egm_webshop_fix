<?php

namespace Drupal\order_total_summary_fix;

use CommerceGuys\Intl\Calculator;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\commerce_price\Price;

class CustomOrderTotalSummary implements OrderTotalSummaryInterface {

  /**
   * The adjustment transformer.
   *
   * @var \Drupal\commerce_order\AdjustmentTransformerInterface
   */
  protected $adjustmentTransformer;

  /**
   * Constructs a new OrderTotalSummary object.
   *
   * @param \Drupal\commerce_order\AdjustmentTransformerInterface $adjustment_transformer
   *   The adjustment transformer.
   */
  public function __construct(AdjustmentTransformerInterface $adjustment_transformer) {
    $this->adjustmentTransformer = $adjustment_transformer;
  }

  /**
   * {@inheritdoc}
   */
  public function buildTotals(OrderInterface $order) {
    $adjustments = $order->collectAdjustments();
    $adjustments = $this->adjustmentTransformer->processAdjustments($adjustments);
    // Included adjustments are not displayed to the customer, they
    // exist to allow the developer to know what the price is made of.
    // The one exception is taxes, which need to be shown for legal reasons.
    $adjustments = array_filter($adjustments, function (Adjustment $adjustment) {
      return $adjustment->getType() == 'tax' || !$adjustment->isIncluded();
    });
    // Convert the adjustments to arrays.
    $adjustments = $this->combineAdjustments($adjustments);

    $adjustments = array_map(function (Adjustment $adjustment) {
      return $adjustment->toArray();
    }, $adjustments);

    $totalPrice = $order->getTotalPrice();
    $vatPrice = new Price('0', $totalPrice->getCurrencyCode());

    $nonTaxAdjustments = [];
    $adjustmentsTax = [];

    $isTaxCalculated = FALSE;
    foreach ($adjustments as $index => $adjustment) {
      if ($adjustment['type'] === 'tax') {
        $isTaxCalculated = TRUE;
        break;
      }
    }

    // Provide the "total" key for backwards compatibility reasons.
    $taxesPerPercentage = [];
    foreach ($adjustments as $index => $adjustment) {
      $adjustments[$index]['total'] = $adjustments[$index]['amount'];

      if ($adjustment['type'] === 'tax') {
        $vatPrice = $vatPrice->add($adjustment['amount']);
        $adjustmentsTax[] = $adjustment;

        $percentage = $adjustment['percentage'] ?? '';
        if ($percentage !== '') {
          $percentage = Calculator::multiply($percentage, 100);
        }
        if (empty($taxesPerPercentage[$percentage])) {
          $taxesPerPercentage[$percentage] = new Price('0', $totalPrice->getCurrencyCode());
        }
        $taxesPerPercentage[$percentage] = $taxesPerPercentage[$percentage]->add($adjustment['amount']);
      }
      else {
        if ($adjustment['type'] === 'shipping') {
          $shippingLabel = 'Shipping';

          if ($isTaxCalculated) {
            $isPriceWithVat = !empty($order->getStore()->get('prices_include_tax')->getValue()[0]['value']);

            $shippingLabel = ($isPriceWithVat) ?
              'Shipping' :
              'Shipping (without VAT)';
          }

          $adjustments[$index]['label'] = $shippingLabel;
          $adjustment['label'] = $shippingLabel;
        }

        $nonTaxAdjustments[] = $adjustment;

      }

    }

    ksort($taxesPerPercentage);

    return [
      'is_tax_calculated' => $isTaxCalculated,
      'subtotal' => $order->getSubtotalPrice(),
      'totalnovat' => $totalPrice->subtract($vatPrice),
      'vat_price' => $vatPrice,
      'adjustments' => $adjustments,
      'total' => $totalPrice,
      'adjustments_non_tax' => $nonTaxAdjustments,
      'adjustments_tax' => $adjustmentsTax,
      'taxes_per_percentage' => $taxesPerPercentage,
    ];
  }

  protected function combineAdjustments(array $adjustments) {
    $combined_adjustments = [];
    foreach ($adjustments as $index => $adjustment) {
      $type = $adjustment->getType();

      // Adókat nem vonjuk össze.
      if ($type === 'tax') {
        $combined_adjustments[] = $adjustment;
        continue;
      }

      $source_id = ($type === 'shipping') ? 'Shipping' : $adjustment->getLabel();
      if (empty($source_id)) {
        // Adjustments without a source ID are always shown standalone.
        $key = $index;
      }
      else {
        // Adjustments with the same type and source ID are combined.
        $key = $type . '_' . $source_id;
      }

      if (empty($combined_adjustments[$key])) {
        $combined_adjustments[$key] = $adjustment;
      }
      else {
        $combined_adjustments[$key] = $this->add($combined_adjustments[$key], $adjustment);
      }
    }
    // The keys used for combining are irrelevant to the caller.
    $combined_adjustments = array_values($combined_adjustments);

    return $combined_adjustments;
  }

  protected function add(Adjustment $adjustment1, Adjustment $adjustment2) {
    $definition = [
        'amount' => $adjustment1->getAmount()->add($adjustment2->getAmount()),
      ] + $adjustment1->toArray();

    return new Adjustment($definition);
  }

}
