<?php

namespace Drupal\custom_checkout\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;

/**
 * @CommerceCheckoutFlow(
 *  id = "egm_checkout_flow",
 *  label = @Translation("EGM Checkout Flow"),
 * )
 */
class EgmCheckoutFlow extends CheckoutFlowWithPanesBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    // Note that previous_label and next_label are not the labels
    // shown on the step itself. Instead, they are the labels shown
    // when going back to the step, or proceeding to the step.
    return [
        'admin_choose_customer' => [
          'label' => $this->t('Choose customer'),
          'previous_label' => $this->t('Go back'),
          'next_label' => $this->t('Go to next step'),
          'has_sidebar' => FALSE,
          'has_order_summary' => FALSE,
        ],

        'login' => [
          'label' => $this->t('Login'),
          'previous_label' => $this->t('Go back'),
          'next_label' => $this->t('Go to next step'),
          'has_sidebar' => FALSE,
          'has_order_summary' => FALSE,
        ],

        'order_information' => [
          'label' => $this->t('Order information'),
          'previous_label' => $this->t('Go back'),
          'next_label' => $this->t('Go to next step'),
          'has_sidebar' => TRUE,
          'has_order_summary' => TRUE,
        ],

        'review_order' => [
          'label' => $this->t('Review'),
          'previous_label' => $this->t('Go back'),
          'next_label' => $this->t('Go to next step'),
          'has_sidebar' => TRUE,
          'has_order_summary' => TRUE,
        ],

        'review' => [
          'label' => $this->t('Payment'),
          'previous_label' => $this->t('Go back'),
          'next_label' => $this->t('Pay and complete purchase'),
          'has_sidebar' => TRUE,
          'has_order_summary' => TRUE,
        ],

      ] + parent::getSteps();
  }

}
