<?php

namespace Drupal\custom_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;

/**
 * Provides the review pane.
 *
 * @CommerceCheckoutPane(
 *   id = "payment_buttons",
 *   label = @Translation("Payment Buttons"),
 *   display_label = @Translation("Payment"),
 *   default_step = "review",
 * )
 */
class PaymentButtons extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['userdata_usage_info'] = [
      '#theme' => 'payment_buttons_pane',
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $paymentGateways = $this->order->get('payment_gateway')->referencedEntities();
    $paymentGateway = reset($paymentGateways);

    if ($paymentGateway instanceof PaymentGatewayInterface  && $paymentGateway->getPluginId() !== 'manual') {
      return TRUE;
    }

    return FALSE;
  }
}
