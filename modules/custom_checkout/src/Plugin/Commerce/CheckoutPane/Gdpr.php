<?php

namespace Drupal\custom_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the review pane with GDPR agreement to the use of my personal data.
 *
 * @CommerceCheckoutPane(
 *   id = "gdpr",
 *   label = @Translation("GDPR"),
 *   display_label = @Translation("Data management"),
 *   default_step = "review",
 * )
 */
class Gdpr extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->order->hasField('field_gdpr')) {
      $gdprFieldDefinition = $this->order->getFieldDefinition('field_gdpr');
      $gdprLabel = $gdprFieldDefinition->getLabel();
      $gdprDescription = $gdprFieldDefinition->getDescription();

      $pane_form['field_gdpr'] = [
        '#type' => 'checkbox',
        '#title' => $gdprLabel,
        '#description' => $gdprDescription,
        '#required' => TRUE,
      ];
    }
    else {
      \Drupal::logger('custom_checkout')
        ->error('Missing field_gdpr boolean field from the order type.');
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
    $paneFormSubmittedValues = $form_state->getValue('gdpr');
    if (isset($paneFormSubmittedValues['field_gdpr'])) {
      $this->order->set(
        'field_gdpr',
        $paneFormSubmittedValues['field_gdpr']
      );
    }
  }

}
