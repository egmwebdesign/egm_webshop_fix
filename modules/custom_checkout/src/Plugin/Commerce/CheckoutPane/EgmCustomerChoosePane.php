<?php

namespace Drupal\custom_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\user\Entity\User;

/**
 * @CommerceCheckoutPane(
 *  id = "egm_customer_choose",
 *  display_label = @Translation("Choose customer"),
 *  label = @Translation("Choose customer - EGM"),
 *  default_step = "admin_choose_customer",
 * )
 */
class EgmCustomerChoosePane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $users = \Drupal::entityTypeManager()->getStorage("user")->loadMultiple();
    $options = [];
    $currentUserId = \Drupal::currentUser()->id();

    foreach ($users as $user) {
      $userId = $user->id();
      if ($userId == 0) {
        continue;
      }
      $options[$userId] = $user->getAccountName();
    }

    $options[$currentUserId] = $options[$currentUserId] . ' ' . $this->t('(for me)');

    $pane_form['user_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose customer'),
      '#options' => $options,
      '#default_value' => $currentUserId,
      '#required' => TRUE,
    ];

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$form, FormStateInterface $form_state, array &$complete_form) {
    $formUserInput = $form_state->getUserInput();

    if (!isset($formUserInput["egm_customer_choose"]["user_select"])) {
      $form_state->setError($form['user_select'],
        t('Please, select a customer!'));
    }

    $userId = $formUserInput["egm_customer_choose"]["user_select"];

    $user = User::load($userId);

    if (empty($user)) {
      $form_state->setError($form['user_select'],
        t('Please, select a customer!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $formUserInput = $form_state->getUserInput();
    $userId = isset($formUserInput["egm_customer_choose"]["user_select"]) ?
      $formUserInput["egm_customer_choose"]["user_select"] : 1;

    // Csak akkor mentsünk, ha más van kiválasztva, mint ami az orderbe mentve
    // van.
    if ($this->order->getCustomerId() === $userId) {
      return;
    }

    $user = User::load($userId);
    $this->order->setCustomerId($userId);

    $email = $user->getEmail();
    $this->order->setEmail($email);
    $this->order->set('billing_profile', NULL);
    $this->order->shipments = [];

  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $user = User::load(\Drupal::currentUser()->id());
    return $user->hasPermission('administer commerce_order');
  }

}
