<?php

namespace Drupal\egm_webshop_fix;

use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines functions for helping product variation price calculations.
 *
 * We need that for displaying promotions.
 */
interface OrderHelperInterface {

  /**
   * Creates a fake Order to be available to calculate Prices later.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $purchasable_entity
   *   Product variation.
   * @param int $quantity
   *   Order quantity.
   * @param \Drupal\commerce\Context $context
   *   Context describing the current user and store.
   * @param \Drupal\commerce_price\Price $price
   *   Optional: you can set custom price if we want to replace
   *   the variation's price in the calculations.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Created fake order.
   */
  public function createFakeOrderForCalculations(ProductVariationInterface $purchasable_entity, $quantity, Context $context, $price = NULL);

  /**
   * Returns the current context.
   *
   * @return \Drupal\commerce\Context
   *   Context describing the current user and store.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   Optional: you can set the current store manually.
   */
  public function getContext($currentStore = NULL);

  /**
   * Returns the calculated price for the first order item in the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $adjustments
   *   Adjustments array.
   *
   * @return \Drupal\commerce_price\Price|null
   *   Calculated price or NULL in case of missing order or product variation.
   */
  public function getFirstProductVariationCalculatedPrice(OrderInterface $order, array $adjustments = []);

  /**
   * Returns the adjustment types.
   *
   * @param bool $feesEnabled
   *   If this is true, fees will be in the result array.
   * @param bool $taxesEnabled
   *   If this is true, taxes will be in the result array.
   * @param bool $promotionsEnabled
   *   If this is true, promotions will be in the result array.
   * @param bool $shippingEnabled
   *   If this is true, shipping will be in the result array.
   * @param bool $shippingPromotionEnabled
   *   If this is true, shipping_promotion will be in the result array.
   *
   * @return array
   *   Adjustment types.
   */
  public function getAdjustmentTypes($feesEnabled, $taxesEnabled, $promotionsEnabled, $shippingEnabled, $shippingPromotionEnabled);

  /**
   * Adds an order processor.
   *
   * @param \Drupal\commerce_order\OrderProcessorInterface $processor
   *   The order processor.
   */
  public function addProcessor(OrderProcessorInterface $processor);

}
