<?php

namespace Drupal\egm_webshop_fix;

use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce\Context;
use Drupal\commerce_order\Resolver\ChainOrderTypeResolverInterface;
use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Helps creating fake orders to calculate prices with adjustments.
 */
class OrderHelper implements OrderHelperInterface {
  /**
   * The adjustment transformer.
   *
   * @var \Drupal\commerce_order\AdjustmentTransformerInterface
   */
  protected $adjustmentTransformer;

  /**
   * The chain order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\ChainOrderTypeResolverInterface
   */
  protected $chainOrderTypeResolver;

  /**
   * The chain price resolver.
   *
   * @var \Drupal\commerce_price\Resolver\ChainPriceResolverInterface
   */
  protected $chainPriceResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The order processors.
   *
   * @var \Drupal\commerce_order\OrderProcessorInterface[]
   */
  protected $processors = [];

  /**
   * The unsaved orders used for calculations.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface[]
   */
  protected $orders = [];

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Currently used store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * Price calculator for calculating product variation prices.
   *
   * @var \Drupal\commerce_order\PriceCalculatorInterface
   */
  protected $priceCalculator;

  /**
   * Constructs a new OrderHelper object.
   *
   * @param \Drupal\commerce_order\AdjustmentTransformerInterface $adjustmentTransformer
   *   The adjustment transformer.
   * @param \Drupal\commerce_order\Resolver\ChainOrderTypeResolverInterface $chain_order_type_resolver
   *   The chain order type resolver.
   * @param \Drupal\commerce_price\Resolver\ChainPriceResolverInterface $chain_price_resolver
   *   The chain price resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   Currently used store.
   * @param \Drupal\commerce_order\PriceCalculatorInterface $priceCalculator
   *   Price calculator for calculating product variation prices.
   */
  public function __construct(AdjustmentTransformerInterface $adjustmentTransformer, ChainOrderTypeResolverInterface $chain_order_type_resolver, ChainPriceResolverInterface $chain_price_resolver, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, AccountInterface $currentUser, CurrentStoreInterface $currentStore, PriceCalculatorInterface $priceCalculator) {
    $this->adjustmentTransformer = $adjustmentTransformer;
    $this->chainOrderTypeResolver = $chain_order_type_resolver;
    $this->chainPriceResolver = $chain_price_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->currentUser = $currentUser;
    $this->currentStore = $currentStore;
    $this->priceCalculator = $priceCalculator;
  }

  /**
   * {@inheritdoc}
   */
  public function createFakeOrderForCalculations(ProductVariationInterface $purchasable_entity, $quantity, Context $context, $price = NULL) {
    $resolved_price = (isset($price)) ?
      $price :
      $this->chainPriceResolver->resolve($purchasable_entity, $quantity, $context);

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->entityTypeManager->getStorage('commerce_order_item');
    $order_item = $order_item_storage->createFromPurchasableEntity($purchasable_entity);
    $order_item->setUnitPrice($resolved_price, TRUE);
    $order_item->setQuantity($quantity);
    $order_type_id = $this->chainOrderTypeResolver->resolve($order_item);

    /** @var OrderInterface $order */
    $order = $this->prepareOrder($order_type_id, $context);
    $order_item->order_id = $order;
    $order->setItems([$order_item]);

    // Allow each selected processor to add its adjustments.
    foreach ($this->processors as $processor) {
      $processor->process($order);
    }
    $calculated_price = $order_item->getAdjustedTotalPrice();
    $adjustments = $order_item->getAdjustments();
    $adjustments = $this->adjustmentTransformer->processAdjustments($adjustments);

    return $order;
  }

  /**
   * Prepares an unsaved order for the given type/context.
   */
  private function prepareOrder($order_type_id, Context $context) {
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $this->orders[$order_type_id] = $order_storage->create([
      'type' => $order_type_id,
      'ip_address' => $this->requestStack->getCurrentRequest()->getClientIp(),
      // Provide a flag that can be used in the order create hook/event
      // to identify orders used for price calculation purposes.
      'data' => ['provider' => 'order_price_calculator'],
    ]);

    $order = $this->orders[$order_type_id];
    // Make sure that the order data matches the data passed in the context.
    $order->setStoreId($context->getStore()->id());
    $order->setCustomerId($context->getCustomer()->id());
    $order->setEmail($context->getCustomer()->getEmail());
    // Start from a clear set of adjustments each time.
    $order->clearAdjustments();

    return $order;
  }

  /**
   * {@inheritdoc}
   */
  public function addProcessor(OrderProcessorInterface $processor) {
    $this->processors[] = $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstProductVariationCalculatedPrice(OrderInterface $order, array $adjustments = []) {
    if (!isset($order)) {
      return NULL;
    }

    $orderItems = $order->getItems();
    if (isset($orderItems) && count($orderItems) > 0) {
      $order_item = $orderItems[0];
      $calculated_price = $order_item->getAdjustedTotalPrice($adjustments);
      return $calculated_price;
    }

    // In case of missing product variations, return NULL.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($currentStore = NULL) {
    if (empty($currentStore)) {
      $currentStore = $this->currentStore->getStore();
    }
    return new Context($this->currentUser, $currentStore);
  }

  /**
   * {@inheritdoc}
   */
  public function getAdjustmentTypes($feesEnabled, $taxesEnabled, $promotionsEnabled, $shippingEnabled, $shippingPromotionEnabled) {
    $result = [];

    if ($feesEnabled) {
      $result['fee'] = 'fee';
    }

    if ($taxesEnabled) {
      $result['tax'] = 'tax';
    }

    if ($promotionsEnabled) {
      $result['promotion'] = 'promotion';
    }

    if ($shippingEnabled) {
      $result['shipping'] = 'shipping';
    }

    if ($shippingPromotionEnabled) {
      $result['shipping_promotion'] = 'shipping_promotion';
    }

    return $result;
  }

}
